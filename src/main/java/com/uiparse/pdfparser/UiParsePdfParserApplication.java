package com.uiparse.pdfparser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class UiParsePdfParserApplication {
	public static void main(String[] args) {
		SpringApplication.run(UiParsePdfParserApplication.class, args);
	}
}

