package com.uiparse.pdfparser.filter;

import com.uiparse.pdfparser.service.HelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by md_al on 12-Jan-19.
 */
@Component
@Order(1)
public class RequestResponseFilter implements Filter {

    @Autowired
    private HelperService helperService;

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        HttpSession session = ((HttpServletRequest) request).getSession();
        //helperService.updateSession(session, "successMessage", null);
        //helperService.updateSession(session, "errorMessage", null);
        chain.doFilter(request, response);

    }

    // other methods
}
