package com.uiparse.pdfparser.dao;

import com.uiparse.pdfparser.db.Parser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Created by md_al on 01-Jan-19.
 */
public interface ParserDao extends CrudRepository<Parser, Integer> {

    @Query("select u from Parser u where u.createdBy=:createdBy and name is not null")
    List<Parser> findAllByCreatedBy(@Param("createdBy") Integer createdBy);

    @Query("select u from Parser u where u.id=:id")
    Parser findOneById(@Param("id") Integer id);
}



