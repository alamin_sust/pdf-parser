package com.uiparse.pdfparser.dao;

import com.uiparse.pdfparser.db.ParsedFile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by md_al on 01-Jan-19.
 */
public interface ParsedFileDao extends CrudRepository<ParsedFile, Integer> {

    @Query("select u from ParsedFile u where uploadedBy=:uploadedBy and bestParserId is not null")
    public List<ParsedFile> findAllByUploadedBy(@Param("uploadedBy") int uploadedBy);

    @Query("select u from ParsedFile u where uploadedBy=:uploadedBy and bestParserId is not null and accuracy>=:minAccuracy")
    public List<ParsedFile> findAllByUploadedByAndMinAccuracy(@Param("uploadedBy") int uploadedBy, @Param("minAccuracy") double minAccuracy);

    @Query("select u from ParsedFile u where uploadedBy=:uploadedBy and bestParserId is not null and accuracy<:maxAccuracy")
    public List<ParsedFile> findAllByUploadedByAndMaxAccuracy(@Param("uploadedBy") int uploadedBy, @Param("maxAccuracy") double maxAccuracy);

    @Modifying
    @Transactional
    @Query("delete from ParsedFile where bestParserId=:bestParserId")
    void deleteByBestParserId(@Param("bestParserId") int bestParserId);
}



