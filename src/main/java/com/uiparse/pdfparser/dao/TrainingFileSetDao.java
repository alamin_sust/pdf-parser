package com.uiparse.pdfparser.dao;

import com.uiparse.pdfparser.db.TrainingFileSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by md_al on 12-Jan-19.
 */
@Repository
public interface TrainingFileSetDao extends JpaRepository<TrainingFileSet, Integer> {

    @Query("select u from TrainingFileSet u where u.parserId=:parserId and uploadedBy=:uploadedBy")
    public List<TrainingFileSet> findAllByParserIdAndUploadedBy(@Param("parserId") int parserId, @Param("uploadedBy") int uploadedBy);

    @Modifying
    @Transactional
    @Query("delete from TrainingFileSet where parserId=:parserId")
    void deleteByParserId(@Param("parserId") int parserId);
}
