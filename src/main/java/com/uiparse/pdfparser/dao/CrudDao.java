package com.uiparse.pdfparser.dao;

import com.uiparse.pdfparser.connection.Database;
import com.uiparse.pdfparser.db.TrainingFileSet;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by md_al on 12-Jan-19.
 */
@Repository
public class CrudDao {

    public String getNextTriningId(String userId, String parserId) throws SQLException {
        Database db = new Database();
        db.connect();

        try {
            String q = "select max(id)+1 mxId from training_file_set";

            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            if (rs.next()) {
                return rs.getString("mxId") == null ? "1" : rs.getString("mxId");
            } else {
                return "1";
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }

        return "1";
    }

    public String getNextParserId() throws SQLException {

        Database db = new Database();
        db.connect();

        try {
            String q = "select max(id)+1 mxId from parser";
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            if (rs.next()) {
                return rs.getString("mxId") == null ? "1" : rs.getString("mxId");
            } else {
                return "1";
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }

        return "1";
    }

    public void insertIntoTrainingFileSetTable(String userId, String parserId, String nextId) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            String q = "insert into training_file_set (id, parser_id, uploaded_by, upload_date) " +
                    " values(" + nextId + "," + parserId + "," + userId + ",now())";
            Statement st = db.connection.createStatement();
            st.executeUpdate(q);
        } catch (Exception e) {

        } finally {
            db.close();
        }
    }

    public List<TrainingFileSet> findAllTrainingFileSetByParserId(int parserId) throws SQLException {
        Database db = new Database();
        db.connect();
        List<TrainingFileSet> trainingFileSetList = new ArrayList<>();
        try {
            String q = "select * from training_file_set where parser_id = " + parserId;
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);


            while (rs.next()) {
                TrainingFileSet trainingFileSet = new TrainingFileSet();
                trainingFileSet.setId(rs.getInt("id"));
                trainingFileSet.setParserId(rs.getInt("parser_id"));
                trainingFileSet.setUploadedBy(rs.getInt("uploaded_by"));
                trainingFileSet.setUploadDate(rs.getDate("upload_date"));
            }
        } catch (Exception e) {

        } finally {
            db.close();
        }
        return trainingFileSetList;
    }

    public void insertIntoFinalMapTable(String parserId, boolean cellValueStatic, int mainFileRowIndex, int mainFileColIndex,
                                        int resultRowIndex, int resultColIndex) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            String q = "insert into result_mapping (parser_id, row_index, col_index, main_file_row_index, main_file_col_index, is_static) " +
                    " values(" + parserId + "," + resultRowIndex + "," + resultColIndex + "," + mainFileRowIndex + ","
                    + mainFileColIndex + "," + (cellValueStatic == true ? 1 : 0) + ")";
            Statement st = db.connection.createStatement();
            st.executeUpdate(q);
        } catch (Exception e) {

        } finally {
            db.close();
        }

    }

    public List<String> findAllFromResultMappingsByParserId(String parserId) throws SQLException {
        Database db = new Database();
        db.connect();
        List<String> concatStr = new LinkedList<>();
        try {
            String q = "select concat(is_static , '#' , row_index , '_' , col_index , '_' , main_file_row_index , '_' , main_file_col_index) as concatStr from result_mapping where parser_id = " + parserId;
            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);

            while (rs.next()) {
                concatStr.add(rs.getString("concatStr"));
            }

        } catch (Exception e) {

        } finally {
            db.close();
        }
        return concatStr;
    }
}
