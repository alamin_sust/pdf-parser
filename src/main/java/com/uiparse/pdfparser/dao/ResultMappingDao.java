package com.uiparse.pdfparser.dao;

import com.uiparse.pdfparser.db.ResultMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by md_al on 12-Jan-19.
 */
@Repository
public interface ResultMappingDao extends JpaRepository<ResultMapping, Integer> {

    @Modifying
    @Transactional
    @Query("delete from ResultMapping where parserId=:parserId")
    void deleteByParserId(@Param("parserId") int parserId);
}
