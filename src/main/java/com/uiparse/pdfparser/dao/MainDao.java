package com.uiparse.pdfparser.dao;

import com.uiparse.pdfparser.db.TrainingFileSet;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by md_al on 01-Jan-19.
 */
public interface MainDao extends CrudRepository<TrainingFileSet, Integer> {

}
