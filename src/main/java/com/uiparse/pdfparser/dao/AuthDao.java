package com.uiparse.pdfparser.dao;

import com.uiparse.pdfparser.db.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by md_al on 12-Jan-19.
 */
@Repository
public interface AuthDao extends JpaRepository<User, Integer> {

    @Query("select u from User u where u.email=:email and u.password=:password")
    public User find(@Param("email") String email,
                           @Param("password") String password);

    @Query("select u from User u where u.email=:email")
    public User find(@Param("email") String email);

    @Query("select u from User u where id=:userId")
    User findOneById(int userId);
}
