package com.uiparse.pdfparser.util;

/**
 * Created by md_al on 12-Jan-19.
 */
public class Constants {
    public static final String LOGOUT_SUCCESS = "Successfully Logged Out.";
    public static final String LOGIN_SUCCESS = "Successfully Logged In.";
    public static final String LOGIN_PASS_MISSMATCH = "Email and Password doesn't macth, please try again.";
    public static final String LOGIN_INVALID_USER_PASS = "Please enter valid email and password";

    public static final String REGISTER_USER_EXISTS = "User Already Exists!";
    public static final String REGISTER_SUCCESS = "Registration Successful!";
    public static final String INSUFFICIENT_PRIVILEGE = "Insufficient Privilege!";

    public static final String TRAINING_FILE_PATH = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\pdf-parser\\src\\main\\webapp\\resources\\training\\";

    public static final String TESTING_FILE_PATH = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\pdf-parser\\src\\main\\webapp\\resources\\testing\\";
    public static final String TESTING_PARSER_AUTO_DETERMINATION_FILE_PATH = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\pdf-parser\\src\\main\\webapp\\resources\\parsed\\";

    public static final String TESTING_RESULT_FILE_PATH = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\pdf-parser\\src\\main\\webapp\\resources\\testing_result\\";
    public static final String TESTING_PARSER_AUTO_DETERMINATION_RESULT_FILE_PATH = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\pdf-parser\\src\\main\\webapp\\resources\\testing_parser_auto_determination_result\\";
    public static final String SAMPLE_META_DATA_FILE_PATH = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\pdf-parser\\src\\main\\webapp\\resources\\sample_meta_data\\";


}
