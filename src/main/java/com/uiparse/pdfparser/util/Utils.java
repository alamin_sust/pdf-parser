package com.uiparse.pdfparser.util;

/**
 * Created by md_al on 12-Jan-19.
 */
public class Utils {
    public static boolean isNullOrEmpty(String val) {
        return val==null || val.equals("");
    }

    public static boolean isValid(String val) {
        return !isNullOrEmpty(val);
    }
}
