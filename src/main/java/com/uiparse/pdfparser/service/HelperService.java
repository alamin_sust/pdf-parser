package com.uiparse.pdfparser.service;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.util.Enumeration;

/**
 * Created by md_al on 12-Jan-19.
 */
@Service
public class HelperService {
    public void updateSession(HttpSession session, String key, String value) {
        session.setAttribute(key, value);
    }

    public boolean isFileTypeSame(MultipartFile file, String requiredFileType) {
        return file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1).equals(requiredFileType);
    }
}
