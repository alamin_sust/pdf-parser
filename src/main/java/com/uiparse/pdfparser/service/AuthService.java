package com.uiparse.pdfparser.service;

import com.uiparse.pdfparser.dao.AuthDao;
import com.uiparse.pdfparser.db.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.List;

import static com.uiparse.pdfparser.util.Constants.*;
import static com.uiparse.pdfparser.util.Utils.isValid;

/**
 * Created by md_al on 12-Jan-19.
 */
@Service
public class AuthService {

    @Autowired
    private HelperService helperService;

    @Autowired
    private AuthDao authDao;

    public void logout(HttpServletRequest request, HttpSession session) {
        if (request.getParameter("logout") != null) {
            helperService.updateSession(session, "successMessage", LOGOUT_SUCCESS);
        }
        helperService.updateSession(session, "id", null);
        helperService.updateSession(session, "email", null);
    }

    public boolean login(HttpServletRequest request, HttpSession session) {

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        if (isValid(email) && isValid(password)) {
            User user = authDao.find(email, password);
            if (user != null) {
                helperService.updateSession(session, "id", String.valueOf(user.getId()));
                helperService.updateSession(session, "email", user.getEmail());
                helperService.updateSession(session, "successMessage", LOGIN_SUCCESS);
                return true;
            } else {
                helperService.updateSession(session, "errorMessage", LOGIN_PASS_MISSMATCH);
                return false;
            }
        } else {
            helperService.updateSession(session, "errorMessage", LOGIN_INVALID_USER_PASS);
            return false;
        }
    }

    public boolean register(HttpServletRequest request, HttpSession session) {

        if(session.getAttribute("email") == null && !session.getAttribute("email").toString().equals("admin@sparkyai.com")) {
            helperService.updateSession(session, "errorMessage", INSUFFICIENT_PRIVILEGE);
            return false;
        }

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        if (isValid(email) && isValid(password)) {
            User user = authDao.find(email);
            if (user == null) {
                user = new User();
                user.setEmail(email);
                user.setPassword(password);
                authDao.save(user);
                helperService.updateSession(session, "successMessage", REGISTER_SUCCESS);
                return true;
            } else {
                helperService.updateSession(session, "errorMessage", REGISTER_USER_EXISTS);
                return false;
            }
        } else {
            helperService.updateSession(session, "errorMessage", LOGIN_INVALID_USER_PASS);
            return false;
        }
    }
}
