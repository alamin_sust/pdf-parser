package com.uiparse.pdfparser.service;

import com.uiparse.pdfparser.dao.CrudDao;
import com.uiparse.pdfparser.dao.ParsedFileDao;
import com.uiparse.pdfparser.dao.ParserDao;
import com.uiparse.pdfparser.db.ExcelRow;
import com.uiparse.pdfparser.db.MetaData;
import com.uiparse.pdfparser.db.ParsedFile;
import com.uiparse.pdfparser.db.Parser;
import com.uiparse.pdfparser.util.Constants;
import com.uiparse.pdfparser.util.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.eadge.extractpdfexcel.PdfConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by md_al on 05-Jan-19.
 */
@Service
public class FileService {

    @Autowired
    private CrudDao crudDao;

    @Autowired
    private ParserDao parserDao;

    @Autowired
    private ParsedFileDao parsedFileDao;

    @Autowired
    private MainService mainService;

    public boolean saveFile(MultipartFile file, String filePath, String fileName) {

        if (file == null) {
            return false;
        }

        boolean success = true;

        InputStream inputStream = null;
        OutputStream outputStream = null;
        //String fileName = file.getOriginalFilename();
        File newFile = new File(filePath
                + fileName
                + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")));

        try {
            inputStream = file.getInputStream();

            if (!newFile.exists()) {
                newFile.createNewFile();
            }
            outputStream = new FileOutputStream(newFile);
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }

        return success;
    }

    public void downloadFile(HttpServletResponse response, String filePath, String fileName) {
        //System.out.println("sfilepath in jsp file is........."+sFile);
        String mimeType = FileUtils.getMimeType(fileName);
        //System.out.println(mimeType);
        //response.setContentType(mimeType);
        //response.setHeader("Content-Disposition", "inline;filename=\"" + sFile + "\"");
        response.setContentType(mimeType);
        response.setHeader("Content-disposition", "attachment;filename=\"" + fileName + "\"");

        FileInputStream in = null;
        OutputStream output = null;
        try {
            output = response.getOutputStream();
            in = new FileInputStream(filePath + fileName);
            byte[] buffer = new byte[5 * 1024];
            int size = 0;
            while ((size = in.read(buffer, 0, buffer.length)) > 0) {
                output.write(buffer, 0, size);
            }


        } catch (Exception e) {
        } finally {
            try {
                in.close();
                output.flush();
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean writeExcelContentRaw(String[][] excelRowList, String filePath, String fileName) throws FileNotFoundException, IOException {
        // Create a Workbook
        Workbook workbook = new HSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Report");


        //style
        CellStyle normal_style = workbook.createCellStyle();
        CellStyle err_style = workbook.createCellStyle();

        /* First, let us draw a thick border so that the color is visible */
        err_style.setBorderLeft(CellStyle.BORDER_THICK);
        err_style.setBorderRight(CellStyle.BORDER_THICK);
        err_style.setBorderTop(CellStyle.BORDER_THICK);
        err_style.setBorderBottom(CellStyle.BORDER_THICK);

        /* We will use IndexedColors to specify colors to the border */
        /* bottom border color */
        err_style.setBottomBorderColor(IndexedColors.RED.getIndex());
        /* Top border color */
        err_style.setTopBorderColor(IndexedColors.RED.getIndex());
        /* Left border color */
        err_style.setLeftBorderColor(IndexedColors.RED.getIndex());
        /* Right border color */
        err_style.setRightBorderColor(IndexedColors.RED.getIndex());
        //end style


        DataFormat fmt = workbook.createDataFormat();
        normal_style.setDataFormat(fmt.getFormat("@"));
        err_style.setDataFormat(fmt.getFormat("@"));


        /*// Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        List<String> excelHeaderList = Statics.getExcelHeaderList();
        // Create cells
        for(int i = 0; i < excelHeaderList.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(excelHeaderList.get(i));
            cell.setCellStyle(headerCellStyle);
        }*/

        // Create Cell Style for formatting Date
        //CellStyle dateCellStyle = workbook.createCellStyle();
        //dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        // Create Other rows and cells with employees data
        int rowNum = 0;
        for (String[] colList : excelRowList) {
            Row row = sheet.createRow(rowNum++);

            for (int i = 0; i < colList.length; i++) {
                Cell cell = row.createCell(i);
                        cell.setCellStyle((colList[i]!=null && colList[i].trim().equals("???")) ? err_style : normal_style);
                        cell.setCellValue(colList[i] == null? null: colList[i].trim());
            }


            //Cell dateOfBirthCell = row.createCell(2);
            //dateOfBirthCell.setCellValue(employee.getDateOfBirth());
            //dateOfBirthCell.setCellStyle(dateCellStyle);

        }

        /*// Resize all columns to fit the content size
        for(int i = 0; i < excelHeaderList.size(); i++) {
            sheet.autoSizeColumn(i);
        }*/

        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(filePath + fileName);
        workbook.write(fileOut);
        fileOut.close();

        // Closing the workbook
        //workbook.close();

        return true;
    }

    public boolean writeExcelContent(List<ExcelRow> excelRowList, String filePath, String fileName) throws FileNotFoundException, IOException {
        // Create a Workbook
        Workbook workbook = new HSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Report");

        /*// Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        List<String> excelHeaderList = Statics.getExcelHeaderList();
        // Create cells
        for(int i = 0; i < excelHeaderList.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(excelHeaderList.get(i));
            cell.setCellStyle(headerCellStyle);
        }*/

        // Create Cell Style for formatting Date
        //CellStyle dateCellStyle = workbook.createCellStyle();
        //dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        // Create Other rows and cells with employees data
        int rowNum = 0;
        for (ExcelRow excelRow : excelRowList) {
            Row row = sheet.createRow(rowNum++);

            List<String> colList = excelRow.getColList();
            for (int i = 0; i < colList.size(); i++) {
                row.createCell(i)
                        .setCellValue(colList.get(i) == null? null:colList.get(i).trim());
            }


            //Cell dateOfBirthCell = row.createCell(2);
            //dateOfBirthCell.setCellValue(employee.getDateOfBirth());
            //dateOfBirthCell.setCellStyle(dateCellStyle);

        }

        /*// Resize all columns to fit the content size
        for(int i = 0; i < excelHeaderList.size(); i++) {
            sheet.autoSizeColumn(i);
        }*/

        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(filePath + fileName);
        workbook.write(fileOut);
        fileOut.close();

        // Closing the workbook
        //workbook.close();

        return true;
    }



    public boolean parseTextFromPdfAndWriteInExcel(String pdfFileName, String excelFileName) throws IOException {
        boolean success=true;
        try {
            PdfConverter.createExcelFile(pdfFileName, excelFileName);
        } catch (Exception e) {
            success = false;
        }
        return success;
    }

    public void populateModel(Model model, Object id) {
    }

    public void processTrainingFileSet(MultipartFile pdfFile, MultipartFile excelFile, MultipartFile excelMetaDataFile, HttpSession session, String parserId) {
        try {
            String userId = session.getAttribute("id") == null ? "0" : session.getAttribute("id").toString();
            String nextId = crudDao.getNextTriningId(userId, parserId);
            String fileName = userId + "_" + parserId + "_" + nextId;
            crudDao.insertIntoTrainingFileSetTable(userId,parserId,nextId);
            boolean pdfSuccess = saveFile(pdfFile, Constants.TRAINING_FILE_PATH, fileName);
            boolean excelSuccess = saveFile(excelFile, Constants.TRAINING_FILE_PATH, fileName);
            boolean excelMetaDataSuccess = true;
            if(excelMetaDataFile != null && !excelMetaDataFile.getOriginalFilename().equals("")) {
                excelMetaDataSuccess = saveFile(excelMetaDataFile, Constants.TRAINING_FILE_PATH,  userId + "_" + parserId + "_metadata");
            }
            boolean pdfToExcelSuccess = parseTextFromPdfAndWriteInExcel(Constants.TRAINING_FILE_PATH +fileName+".pdf",
                    Constants.TRAINING_FILE_PATH +fileName+"_main.xls");

            if(!(pdfSuccess && excelSuccess && excelMetaDataSuccess && pdfToExcelSuccess)) {
                session.setAttribute("errorMessage", "Unexpected Error Occured, Please Try Again.");
                throw new Exception();
            }
            session.setAttribute("successMessage", "Upload Successful");

        } catch (Exception e) {
            System.out.println("Method processTrainingFileSet: " + e);
        } finally {

        }
    }

    public void processTestingFile(MultipartFile pdfFile, HttpSession session, String parserId) {
        try {
            String userId = session.getAttribute("id") == null ? "0" : session.getAttribute("id").toString();
            String fileName = userId + "_" + parserId;
            boolean pdfSuccess = saveFile(pdfFile, Constants.TESTING_FILE_PATH, fileName);
            boolean pdfToExcelSuccess = parseTextFromPdfAndWriteInExcel(Constants.TESTING_FILE_PATH +fileName+".pdf",
                    Constants.TESTING_FILE_PATH +fileName+"_main.xls");

            if(!(pdfSuccess && pdfToExcelSuccess)) {
                session.setAttribute("errorMessage", "Unexpected Error Occured, Please Try Again.");
                throw new Exception();
            }
            session.setAttribute("successMessage", "Upload Successful");

        } catch (Exception e) {
            System.out.println("Method processTrainingFileSet: " + e);
        } finally {

        }
    }

    public List<ExcelRow> readExcel(String filePath, String fileName, boolean considerEmptyOrNullCells) throws IOException {
        String excelFilePath = filePath + fileName;
        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

        List<ExcelRow> excelRowList = new LinkedList<>();

        Workbook workbook = new HSSFWorkbook(inputStream);

        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            Sheet sheet = workbook.getSheetAt(i);

            /*DataFormat fmt = workbook.createDataFormat();
            CellStyle textStyle = workbook.createCellStyle();
            textStyle.setDataFormat(fmt.getFormat("@"));
            for(int column=0;column<1000;column++) {
                sheet.setDefaultColumnStyle(column, textStyle);
            }
*/
            DataFormat fmt = workbook.createDataFormat();
            CellStyle cellStyle = workbook.createCellStyle();
            cellStyle.setDataFormat(
            fmt.getFormat("@"));

            Iterator<Row> iterator = sheet.iterator();

            int prevCellRowIndex = -1;
            int prevCellColIndex = -1;
            while (iterator.hasNext()) {
                Row nextRow = iterator.next();
                Iterator<Cell> cellIterator = nextRow.cellIterator();

                ExcelRow excelRow = new ExcelRow();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();

                    cell.setCellStyle(cellStyle);

                    int nowCellRowIndex = cell.getRowIndex();
                    int nowCellColIndex = cell.getColumnIndex();
                    if(considerEmptyOrNullCells){
                        addEmptyRowsAndColumnsIfNeeded(excelRowList, excelRow, nowCellRowIndex, nowCellColIndex, prevCellRowIndex, prevCellColIndex);
                    }
                    prevCellRowIndex = cell.getRowIndex();
                    prevCellColIndex = cell.getColumnIndex();
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_STRING:
                            System.out.print(cell.getStringCellValue() == null ? null : cell.getStringCellValue().trim());
                            excelRow.getColList().add(cell.getStringCellValue().replaceAll("\u00A0"," ").trim());
                            break;
                        case Cell.CELL_TYPE_BOOLEAN:
                            System.out.print(cell.getBooleanCellValue());
                            excelRow.getColList().add(String.valueOf(cell.getBooleanCellValue()));
                            break;
                        case Cell.CELL_TYPE_NUMERIC:
                            System.out.print(cell.getNumericCellValue());
                            excelRow.getColList().add(String.valueOf(cell.getNumericCellValue()));
                            break;
                        case Cell.CELL_TYPE_BLANK:
                            System.out.print(cell);
                            excelRow.getColList().add("");
                            break;

                    }
                    System.out.print(" - ");
                }
                System.out.println();
                excelRowList.add(excelRow);
            }
        }

        //workbook.close();
        inputStream.close();
        return excelRowList;
    }

    private void addEmptyRowsAndColumnsIfNeeded(List<ExcelRow> excelRowList, ExcelRow excelRow, int nowCellRowIndex, int nowCellColIndex, int prevCellRowIndex, int prevCellColIndex) {
        if(prevCellRowIndex<nowCellRowIndex) {
            prevCellColIndex = -1;
        }
        for(int i=prevCellRowIndex;i<nowCellRowIndex-1;i++) {
            excelRowList.add(new ExcelRow());
        }

        for(int i=prevCellColIndex;i<nowCellColIndex-1;i++) {
            excelRow.getColList().add("");
        }
    }

    public void processTestingFileAndDetermineParser(MultipartFile pdfFile, HttpSession session) {

        ParsedFile parsedFile = new ParsedFile();
        parsedFile.setUploadedBy(Integer.valueOf(session.getAttribute("id").toString()));
        parsedFile.setUploadDate(new Date());
        parsedFileDao.save(parsedFile);
        String fileName = String.valueOf(parsedFile.getId());
        try {
            boolean pdfSuccess = saveFile(pdfFile, Constants.TESTING_PARSER_AUTO_DETERMINATION_FILE_PATH, fileName);
            boolean pdfToExcelSuccess = parseTextFromPdfAndWriteInExcel(Constants.TESTING_PARSER_AUTO_DETERMINATION_FILE_PATH +fileName+".pdf",
                    Constants.TESTING_PARSER_AUTO_DETERMINATION_FILE_PATH +fileName+"_main.xls");

            if(!(pdfSuccess && pdfToExcelSuccess)) {
                session.setAttribute("errorMessage", "Unexpected Error Occured, Please Try Again.");
                throw new Exception();
            }


            List<Parser> parserList = parserDao.findAllByCreatedBy(Integer.valueOf(session.getAttribute("id").toString()));
            int bestParserId = -1;
            double bestAccuracy = -1.0;
            for(Parser parser: parserList) {
                double accuracy = mainService.trainAndTest2(String.valueOf(parser.getId()), Integer.parseInt(session.getAttribute("id").toString()), String.valueOf(parsedFile.getId()),
                        Constants.TESTING_PARSER_AUTO_DETERMINATION_FILE_PATH, String.valueOf(parsedFile.getId()), Constants.TESTING_PARSER_AUTO_DETERMINATION_RESULT_FILE_PATH);
                if(accuracy > bestAccuracy) {
                    bestAccuracy = accuracy;
                    bestParserId = parser.getId();
                }
            }
            bestAccuracy = mainService.trainAndTest2(String.valueOf(bestParserId),Integer.parseInt(session.getAttribute("id").toString()), String.valueOf(parsedFile.getId()),
                    Constants.TESTING_PARSER_AUTO_DETERMINATION_FILE_PATH, String.valueOf(parsedFile.getId()), Constants.TESTING_PARSER_AUTO_DETERMINATION_RESULT_FILE_PATH);
            parsedFile.setBestParserId(bestParserId);
            parsedFile.setAccuracy(bestAccuracy);
            parsedFileDao.save(parsedFile);

            session.setAttribute("successMessage", "The file has been uploaded successfully!" +
                    " Please check Parsed/Unparsed tab to see the result (file id: "+parsedFile.getId()+")");

        } catch (Exception e) {
            session.setAttribute("errorMessage", "Something Went Wrong, Please Try Again.");
            System.out.println("Method processTrainingFileSet: " + e);
        } finally {

        }
    }
}
