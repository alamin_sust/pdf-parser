package com.uiparse.pdfparser.service;

import com.uiparse.pdfparser.dao.CrudDao;
import com.uiparse.pdfparser.dao.MainDao;
import com.uiparse.pdfparser.dao.TrainingFileSetDao;
import com.uiparse.pdfparser.db.CellInfo;
import com.uiparse.pdfparser.db.ExcelRow;
import com.uiparse.pdfparser.db.MetaData;
import com.uiparse.pdfparser.db.TrainingFileSet;
import com.uiparse.pdfparser.util.Constants;
import com.uiparse.pdfparser.util.Utils;
import org.eadge.extractpdfexcel.PdfConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by md_al on 01-Jan-19.
 */
@Service
public class MainService {

    @Autowired
    FileService fileService;
    @Autowired
    private MainDao mainDao;
    @Autowired
    private CrudDao crudDao;
    @Autowired
    private TrainingFileSetDao trainingFileSetDao;

    public TrainingFileSet getPdf(int id) {
        return mainDao.findById(id).orElse(null);
    }

    /*public void parseTextFromPdfAndWriteInExcel() {
        // Load PDF document
        Document document = new Document(PDF_FILE);
// Instantiate ExcelSave Option object
        ExcelSaveOptions excelsave = new ExcelSaveOptions();
// Save the output to XLS format
        document.save("C://Users/md_al/Documents/NetBeansProjects/pdf-parser/ConvertedFile.xlsx", excelsave);
    }*/

    public void parseTextFromPdfAndWriteInExcel(String pdfFilePath, String excelFilePath) throws IOException {
        PdfConverter.createExcelFile(pdfFilePath, excelFilePath);
    }

    public Double test(String parserId, String fileNamePrefix, String testingFilePath, String resultFileNamePrefix, String resultFilePath) throws IOException, SQLException {

        List<ExcelRow> mainFileExcelRowList = fileService.readExcel(testingFilePath,
                fileNamePrefix + "_main.xls", true);

        List<String> mappings = crudDao.findAllFromResultMappingsByParserId(parserId);

        String[][] arr = new String[1010][255];
        String[][] arrErr = new String[1010][255];
        int probablyMatched = 0;
        int notMatched = 0;
        for (String mapping : mappings) {

            int rowIndex = Integer.parseInt(mapping.split("#")[1].split("_")[0]);
            int colIndex = Integer.parseInt(mapping.split("#")[1].split("_")[1]);
            int mainFileRowIndex = Integer.parseInt(mapping.split("#")[1].split("_")[2]);
            int mainFileColIndex = Integer.parseInt(mapping.split("#")[1].split("_")[3]);

            if (mainFileRowIndex >= 0 && mainFileColIndex >= 0 && mainFileRowIndex < mainFileExcelRowList.size()
                    && mainFileColIndex < mainFileExcelRowList.get(mainFileRowIndex).getColList().size()) {
                arr[rowIndex][colIndex] = arrErr[rowIndex][colIndex] = mainFileExcelRowList.get(mainFileRowIndex).getColList().get(mainFileColIndex);
                if (!Utils.isNullOrEmpty(arr[rowIndex][colIndex])) {
                    probablyMatched++;
                } else {
                    notMatched++;
                    arrErr[rowIndex][colIndex] = "???";
                }
            }
        }
        fileService.writeExcelContentRaw(arr, resultFilePath, resultFileNamePrefix + ".xls");
        fileService.writeExcelContentRaw(arrErr, resultFilePath, resultFileNamePrefix + "_err.xls");
        return mappings.size() > 0 ? ((double) probablyMatched * 100 / (probablyMatched + notMatched)) : 0;
    }

    private Double test2(List<List<CellInfo>> resultList, String parserId, String fileNamePrefix, String testingFilePath, String resultFileNamePrefix, String resultFilePath) throws IOException {
        List<ExcelRow> mainFileExcelRowList = fileService.readExcel(testingFilePath,
                fileNamePrefix + "_main.xls", true);

        Map<String, Set<String>> map = mapStringToIndex(mainFileExcelRowList);

        String[][] arr = new String[1010][255];
        String[][] arrErr = new String[1010][255];
        int probablyMatched = 0;
        int total = 0;

        for (int i = 0; i < resultList.size(); i++) {
            List<CellInfo> row = resultList.get(i);
            for (int j = 0; j < row.size(); j++) {
                total++;
                CellInfo cellInfo = row.get(j);
                if (cellInfo.isStatic()) {
                    probablyMatched++;
                    arr[i][j] = arrErr[i][j] = (cellInfo.getCellLabel()!=null && !cellInfo.getCellLabel().equals(""))?cellInfo.getCellLabel():cellInfo.getCellValue();
                } else {
                    boolean found = false;
                    if (cellInfo.isLeftCellKey()) {
                        System.out.println("topCellValue" + cellInfo.getTopCellValue());
                        Set<String> leftStrSet = map.get(cellInfo.getLeftCellValue());
                        if (leftStrSet != null) {
                            for (String str : leftStrSet) {
                                int x = Integer.parseInt(str.split("_")[0]) + (cellInfo.getKeyDirection().contains("T") ? 1 : 0);
                                int y = Integer.parseInt(str.split("_")[1]) + (cellInfo.getKeyDirection().contains("L") ? 1 : 0)
                                        + cellInfo.getKeyDirection().length() - 1;
                                if (mainFileExcelRowList.size() > x && mainFileExcelRowList.get(x).getColList().size() > y
                                        && mainFileExcelRowList.get(x).getColList().get(y) != null
                                        && !mainFileExcelRowList.get(x).getColList().get(y).equals("")) {
                                    found = true;
                                    String currCellStrs[] = mainFileExcelRowList.get(x).getColList().get(y).trim().split(" ");
                                    String resCellStr = "";
                                    System.out.println("here: " + mainFileExcelRowList.get(x).getColList().get(y).trim());
                                    for (int wordNo = 0; wordNo < cellInfo.getWordCount() && wordNo < currCellStrs.length; wordNo++) {
                                        resCellStr += (wordNo > 0 ? " " : "") + currCellStrs[wordNo];
                                    }
                                    arr[i][j] = arrErr[i][j] = resCellStr;
                                    probablyMatched++;
                                }
                            }
                        }
                    }
                    if (arr[i][j] == null && cellInfo.isTopCellKey()) {
                        System.out.println("topCellValue" + cellInfo.getTopCellValue());
                        Set<String> topStrSet = map.get(cellInfo.getTopCellValue());
                        if (topStrSet != null) {
                            for (String str : topStrSet) {
                                int x = Integer.parseInt(str.split("_")[0]) + (cellInfo.getKeyDirection().contains("T") ? 1 : 0);
                                int y = Integer.parseInt(str.split("_")[1]) + (cellInfo.getKeyDirection().contains("L") ? 1 : 0)
                                        + cellInfo.getKeyDirection().length() - 1;
                                if (mainFileExcelRowList.size() > (x) && mainFileExcelRowList.get(x).getColList().size() > y
                                        && mainFileExcelRowList.get(x).getColList().get(y) != null
                                        && !mainFileExcelRowList.get(x).getColList().get(y).equals("")) {
                                    found = true;
                                    String currCellStrs[] = mainFileExcelRowList.get(x).getColList().get(y).trim().split(" ");
                                    String resCellStr = "";
                                    for (int wordNo = 0; wordNo < cellInfo.getWordCount() && wordNo < currCellStrs.length; wordNo++) {
                                        resCellStr += (wordNo > 0 ? " " : "") + currCellStrs[wordNo];
                                    }
                                    arr[i][j] = arrErr[i][j] = resCellStr;
                                    probablyMatched++;
                                }
                            }
                        }
                    }
                    if (!found) {
                        arrErr[i][j] = "???";
                    }
                }
            }
        }

        fileService.writeExcelContentRaw(arr, resultFilePath, resultFileNamePrefix + ".xls");
        fileService.writeExcelContentRaw(arrErr, resultFilePath, resultFileNamePrefix + "_err.xls");
        return (total) > 0 ? ((double) probablyMatched * 100 / (total)) : 0;
    }

    /*convert to text, not to xls for training. match word by word and store word indexes as to map*/
    public Double trainAndTest2(String parserId, int uploadedBy, String fileNamePrefix, String testingFilePath, String resultFileNamePrefix, String resultFilePath) throws SQLException, IOException {
        List<TrainingFileSet> trainingFileSetList = trainingFileSetDao.findAllByParserIdAndUploadedBy(Integer.parseInt(parserId), uploadedBy);
        List<List<ExcelRow>> trainingFileExcelRowListList = new LinkedList<>();
        List<List<ExcelRow>> mainFileExcelRowList = new LinkedList<>();
        List<List<ExcelRow>> trainingFileExcelRowList = new LinkedList<>();
        List<List<CellInfo>> resultList = new LinkedList<>();
        Integer trainingFileMinColSize[] = new Integer[10010];
        Integer trainingFileMinRowSize = 1000010;
        for (int i = 0; i < trainingFileSetList.size(); i++) {
            TrainingFileSet trainingFileSet = trainingFileSetList.get(i);
            mainFileExcelRowList.add(fileService.readExcel(Constants.TRAINING_FILE_PATH,
                    trainingFileSet.getUploadedBy() + "_" + trainingFileSet.getParserId() + "_" + trainingFileSet.getId() + "_main.xls", true));
            List<ExcelRow> trainingExcelRowList = fileService.readExcel(Constants.TRAINING_FILE_PATH,
                    trainingFileSet.getUploadedBy() + "_" + trainingFileSet.getParserId() + "_" + trainingFileSet.getId() + ".xls", true);
            List<ExcelRow> trainingMetaDataExcelRowList = fileService.readExcel(Constants.TRAINING_FILE_PATH,
                    trainingFileSet.getUploadedBy() + "_" + trainingFileSet.getParserId() + "_metadata" + ".xls", true);

            trainingFileExcelRowList.add(populateMetaDataToExcelRows(trainingExcelRowList, trainingMetaDataExcelRowList));
            trainingFileMinRowSize = Math.min(trainingFileMinRowSize, trainingFileExcelRowList.get(i).size());
        }

        List<Map<String, Set<String>>> stringToIndexMap = mapStringToIndexAllTrainingSet(mainFileExcelRowList);

        for (int i = 0; i < trainingFileSetList.size(); i++) {
            List<ExcelRow> list = trainingFileExcelRowList.get(i);
            for (int j = 0; j < list.size(); j++) {
                if (trainingFileMinColSize[j] == null) {
                    trainingFileMinColSize[j] = list.get(j).getColList().size();
                } else {
                    trainingFileMinColSize[j] = Math.min(trainingFileMinColSize[j], list.get(j).getColList().size());
                }
            }
        }


        for (int i = 0; i < trainingFileMinRowSize; i++) {
            List<CellInfo> resultRow = new LinkedList<>();
            for (int j = 0; j < trainingFileMinColSize[i]; j++) {
                String currentCellStr = trainingFileExcelRowList.get(0).get(i).getColList().get(j);
                MetaData metaData = trainingFileExcelRowList.get(0).get(i).getMetaDataList().get(j);
                Set<String> uniqueStrings = new HashSet<>();
                for (int k = 0; k < trainingFileSetList.size(); k++) {
                    uniqueStrings.add(trainingFileExcelRowList.get(k).get(i).getColList().get(j));
                }
                CellInfo cellInfo = new CellInfo();
                if (
                        //(metaData == null && (uniqueStrings.size() == 1 && trainingFileSetList.size() > 1)) ||
                        (metaData != null && metaData.getCellType().equals("label"))) {
                    cellInfo.setCellValue(currentCellStr == null ? "" : currentCellStr);
                    cellInfo.setCellLabel(metaData == null ? null : metaData.getMetaInfo());
                    cellInfo.setStatic(true);
                    resultRow.add(cellInfo);
                } else {
                    Integer left = null;
                    for (int k = j - 1; k >= 0; k--) {
                        if (resultRow.get(k).isStatic()/*resultList.get(i).get(k).isStatic()*/) {
                            left = k;
                            break;
                        }
                    }
                    Integer top = null;
                    for (int k = i - 1; k >= 0; k--) {
                        if (j < trainingFileMinColSize[k] && resultList.get(k).get(j).isStatic()) {
                            top = k;
                            break;
                        }
                    }
                    cellInfo.setStatic(false);
                    if (currentCellStr == null) {
                        cellInfo.setCellValue("");
                        cellInfo.setCellLabel(null);
                        cellInfo.setWordCount(0);
                    } else if (left != null || top != null) {
                        if (left != null) {
                            cellInfo.setLeftCellValue(resultRow.get(left).getCellValue());
                            cellInfo.setLeftCellKey(true);
                        }
                        if (top != null) {
                            cellInfo.setTopCellValue(resultList.get(top).get(j).getCellValue());
                            cellInfo.setTopCellKey(true);
                        }
                        cellInfo.setWordCount(currentCellStr.trim().split(" ").length);
                        String direction = null;
                        if (left != null) {
                            direction = getCellDirection(mainFileExcelRowList.get(0),
                                    stringToIndexMap.get(0).get(resultRow.get(left).getCellValue() != null ? resultRow.get(left).getCellValue() : resultRow.get(left).getCellValue()), currentCellStr);
                        }
                        if (top != null && direction == null) {
                            direction = getCellDirection(mainFileExcelRowList.get(0), stringToIndexMap.get(0).get(resultList.get(top).get(j).getCellValue() != null ? resultList.get(top).get(j).getCellValue() : resultList.get(top).get(j).getCellValue()), currentCellStr);
                        }
                        if (metaData != null && metaData.getCellType().equals("value") && !metaData.getMetaInfo().equals("")) {
                            if (metaData.getMetaInfo().equals("left")) {
                                cellInfo.setLeftCellKey(true);
                            } else {
                                cellInfo.setTopCellKey(true);
                            }
                        }
                        cellInfo.setKeyDirection(direction == null ? "L" : direction);
                    }
                    resultRow.add(cellInfo);
                }
            }
            resultList.add(resultRow);
        }

        return test2(resultList, parserId, fileNamePrefix, testingFilePath, resultFileNamePrefix, resultFilePath);
    }


    private List<ExcelRow> populateMetaDataToExcelRows(List<ExcelRow> trainingExcelRowList, List<ExcelRow> trainingMetaDataExcelRowList) {

        Map<String, MetaData> metaDataMap = new HashMap<>();
        boolean headerRow = true;
        for (ExcelRow row : trainingMetaDataExcelRowList) {
            if(headerRow) {
                headerRow = false;
                continue;
            }
            MetaData metaData = new MetaData();
            metaData.setCellNumber(row.getColList().size() > 0 ? row.getColList().get(0) : "");
            metaData.setCellType(row.getColList().size() > 1 ? row.getColList().get(1) : "");
            if(metaData.getCellType().equals("label") && row.getColList().size() > 3) {
                metaData.setMetaInfo(row.getColList().get(3));
            } else if(metaData.getCellType().equals("value") && row.getColList().size() > 2) {
                metaData.setMetaInfo(row.getColList().get(2));
            }

            if (!metaData.getCellNumber().equals("") && !metaData.getCellType().equals("")) {
                metaDataMap.put(metaData.getCellNumber(), metaData);
            }
        }

        for (int i = 0; i < trainingExcelRowList.size(); i++) {
            ExcelRow row = trainingExcelRowList.get(i);
            for (int j = 0; j < row.getColList().size(); j++) {
                row.getMetaDataList().add(metaDataMap.get(getCellNumber(i, j)));
            }
        }

        return trainingExcelRowList;
    }

    private String getCellNumber(int i, int j) {
        return Integer.toString(i + 1) + Character.toString((char) ('A' + j));
    }

    private String getCellDirection(List<ExcelRow> excelRowList, Set<String> stringToIndexMap, String currentCellStr) {
        int rr[] = {0, 0, -1, 1};
        int cc[] = {-1, 1, 0, 0};

        if (stringToIndexMap != null) {
            for (String str : stringToIndexMap) {
                int x = Integer.parseInt(str.split("_")[0]);
                int y = Integer.parseInt(str.split("_")[1]);
                if (excelRowList.size() > x && excelRowList.get(x).getColList().size() > y + 1
                        && isCellMatched(excelRowList, x, y + 1, currentCellStr, "goRight") != null) {
                    return "L" + isCellMatched(excelRowList, x, y + 1, currentCellStr, "goRight");

                } else if (excelRowList.size() > x + 1 && excelRowList.get(x + 1).getColList().size() > y
                        && isCellMatched(excelRowList, x + 1, y, currentCellStr, "goDown") != null) {
                    return "T" + isCellMatched(excelRowList, x + 1, y, currentCellStr, "goDown");
                }
            }
        }

        return null;
    }

    private String isCellMatched(List<ExcelRow> excelRowList, int x, int y, String currentCellStr, String goDir) {
        String ret = "";
        if (goDir.equals("goRight")) {
            while (y < excelRowList.get(x).getColList().size() && !(excelRowList.get(x).getColList().get(y) != null && excelRowList.get(x).getColList().get(y).equals(currentCellStr.replace("\n", " ")))) {
                ret += "l";
                y++;
            }
            if (y >= excelRowList.get(x).getColList().size()) return null;
            return ret;
        } else {
            while (x < excelRowList.size() && !(excelRowList.get(x).getColList().size() > y && excelRowList.get(x).getColList().get(y) != null && excelRowList.get(x).getColList().get(y).equals(currentCellStr.replace("\n", " ")))) {
                ret += "u";
                x++;
            }
            if (x >= excelRowList.size()) return null;
            return ret;
        }
    }

    private List<Map<String, Set<String>>> mapStringToIndexAllTrainingSet(List<List<ExcelRow>> mainFileExcelRowList) {
        List<Map<String, Set<String>>> list = new LinkedList<>();
        for (int k = 0; k < mainFileExcelRowList.size(); k++) {

            list.add(mapStringToIndex(mainFileExcelRowList.get(k)));
        }
        return list;
    }

    private Map<String, Set<String>> mapStringToIndex(List<ExcelRow> excelRowList) {
        Map<String, Set<String>> map = new TreeMap<>();
        Set set;
        for (int i = 0; i < excelRowList.size(); i++) {
            ExcelRow nowRow = excelRowList.get(i);
            List<CellInfo> resultRow = new LinkedList<>();
            for (int j = 0; j < nowRow.getColList().size(); j++) {
                String nowCol = nowRow.getColList().get(j);
                if (map.containsKey(nowCol)) {
                    set = map.get(nowCol);
                } else {
                    set = new LinkedHashSet();
                    set.add("" + i + "_" + j);
                    map.put(nowCol, set);
                }
            }
        }
        return map;
    }

    public void train(String parserId, int uploadedBy) throws SQLException, IOException {
        List<TrainingFileSet> trainingFileSetList = trainingFileSetDao.findAllByParserIdAndUploadedBy(Integer.parseInt(parserId), uploadedBy);
        List<List<ExcelRow>> trainingFileExcelRowListList = new LinkedList<>();
        for (TrainingFileSet trainingFileSet : trainingFileSetList) {
            List<ExcelRow> mainFileExcelRowList = fileService.readExcel(Constants.TRAINING_FILE_PATH,
                    trainingFileSet.getUploadedBy() + "_" + trainingFileSet.getParserId() + "_" + trainingFileSet.getId() + "_main.xls", true);
            Map<String, List<CellInfo>> mainFileValueToCellInfoMap = mapValueToCellInfoOfExcelFile(mainFileExcelRowList);

            List<ExcelRow> trainingFileExcelRowList = fileService.readExcel(Constants.TRAINING_FILE_PATH,
                    trainingFileSet.getUploadedBy() + "_" + trainingFileSet.getParserId() + "_" + trainingFileSet.getId() + ".xls", true);

            populateMappedIndexOfMainFile(trainingFileExcelRowList, mainFileValueToCellInfoMap);
            trainingFileExcelRowListList.add(trainingFileExcelRowList);
        }

        mapFinalCellIndex(parserId, trainingFileExcelRowListList);
    }

    private void mapFinalCellIndex(String parserId, List<List<ExcelRow>> trainingFileExcelRowListList) throws SQLException {
        /**Row count and col count must have to be same in each training file**/
        for (int i = 0; i < trainingFileExcelRowListList.get(0).size(); i++) {
            for (int j = 0; j < trainingFileExcelRowListList.get(0).get(i).getColList().size(); j++) {
                List<String> cellValueOfSameIndexInAllFileList = new LinkedList<>();
                List<Integer> cellMappedRowIndexOfMainFileList = new LinkedList<>();
                List<Integer> cellMappedColIndexOfMainFileList = new LinkedList<>();
                Map<String, Integer> indexMap = new TreeMap<>();
                for (int fileNo = 0; fileNo < trainingFileExcelRowListList.size(); fileNo++) {
                    String cellValue = "";
                    int mainFileRowIndex = -1;
                    int mainFileColIndex = -1;
                    try {
                        cellValue = trainingFileExcelRowListList.get(fileNo).get(i).getColList().get(j);
                        mainFileRowIndex = trainingFileExcelRowListList.get(fileNo).get(i).getMappedRowIndexOfMainFileList().get(j);
                        mainFileColIndex = trainingFileExcelRowListList.get(fileNo).get(i).getMappedColIndexOfMainFileList().get(j);
                        cellValueOfSameIndexInAllFileList.add(cellValue);
                    } catch (Exception e) {
                        System.out.println("Exception: CellValue:" + cellValue);
                    }
                    cellMappedRowIndexOfMainFileList.add(mainFileRowIndex);
                    cellMappedColIndexOfMainFileList.add(mainFileColIndex);


                    if (mainFileRowIndex != -1
                            && mainFileColIndex != -1) {
                        if (indexMap.containsKey("" + mainFileRowIndex + "_" + mainFileColIndex)) {
                            indexMap.put("" + mainFileRowIndex + "_" + mainFileColIndex, indexMap.get("" + mainFileRowIndex + "_" + mainFileColIndex) + 1);
                        } else {
                            indexMap.put("" + mainFileRowIndex + "_" + mainFileColIndex, 1);
                        }
                    }

                }
                boolean cellValueStatic = false;
                int finalRowIndex = -1;
                int finalColIndex = -1;
                if (isCellValueStatic(cellValueOfSameIndexInAllFileList)) {
                    cellValueStatic = true;
                } else {
                    int maxVal = 0;
                    for (String key : indexMap.keySet()) {
                        if (indexMap.get(key) > maxVal) {
                            maxVal = indexMap.get(key);
                            finalRowIndex = Integer.parseInt(key.split("_")[0]);
                            finalColIndex = Integer.parseInt(key.split("_")[1]);
                        }
                    }
                }

                crudDao.insertIntoFinalMapTable(parserId, cellValueStatic, finalRowIndex, finalColIndex, i, j);
            }
        }
    }

    private boolean isCellValueStatic(List<String> cellValueOfSameIndexInAllFileList) {

        /*need to do some refactor before using static cell values*/
        return false;

        /*String prevVal = "";
        if(cellValueOfSameIndexInAllFileList.size()<=1) return false;
        for (int i=0;i<cellValueOfSameIndexInAllFileList.size();i++) {
            if(i==0) {
                prevVal = cellValueOfSameIndexInAllFileList.get(i);
                continue;
            }

            if(!prevVal.equals(cellValueOfSameIndexInAllFileList.get(i))) {
                return false;
            }
            prevVal = cellValueOfSameIndexInAllFileList.get(i);
        }
        return true;*/
    }

    private void populateMappedIndexOfMainFile(List<ExcelRow> trainingFileExcelRowList, Map<String, List<CellInfo>> mainFileValueToCellInfoMap) {
        for (int i = 0; i < trainingFileExcelRowList.size(); i++) {
            for (int j = 0; j < trainingFileExcelRowList.get(i).getColList().size(); j++) {
                String cellValue = trainingFileExcelRowList.get(i).getColList().get(j) == null ? null : trainingFileExcelRowList.get(i).getColList().get(j).trim();
                if (mainFileValueToCellInfoMap.containsKey(processString(cellValue))) {
                    trainingFileExcelRowList.get(i).getMappedRowIndexOfMainFileList().add(mainFileValueToCellInfoMap.get(cellValue).get(0).getRowIndex());
                    trainingFileExcelRowList.get(i).getMappedColIndexOfMainFileList().add(mainFileValueToCellInfoMap.get(cellValue).get(0).getColIndex());
                } else {
                    trainingFileExcelRowList.get(i).getMappedRowIndexOfMainFileList().add(-1);
                    trainingFileExcelRowList.get(i).getMappedColIndexOfMainFileList().add(-1);
                }
            }
        }
    }

    private String processString(String cellValue) {
        return cellValue;
    }


    /**
     * This method returns a map of the cellValue to indexList
     * of the main parsed excel file(excel generated from the main pdf).
     **/
    private Map<String, List<CellInfo>> mapValueToCellInfoOfExcelFile(List<ExcelRow> excelRowList) {
        Map<String, List<CellInfo>> map = new TreeMap<>();
        for (int i = 0; i < excelRowList.size(); i++) {
            int maxRow = excelRowList.size();
            for (int j = 0; j < excelRowList.get(i).getColList().size(); j++) {
                int maxCol = excelRowList.get(i).getColList().size();
                String cellValue = excelRowList.get(i).getColList().get(j) == null ? null : excelRowList.get(i).getColList().get(j).trim();
                //String cellLabel = excelRowList.get(i).getMetaDataList().get(j).getLabel();

                List<CellInfo> cellInfoList = new LinkedList<>();
                if (!map.containsKey(cellValue)) {
                    cellInfoList = new LinkedList<>();
                } else {
                    cellInfoList = map.get(cellValue);
                }

                CellInfo cellInfo = new CellInfo();
                cellInfo.setCellValue(cellValue);
                //cellInfo.setCellLabel(cellLabel);
                cellInfo.setRowIndex(i);
                cellInfo.setColIndex(j);
                //cellInfo.setTopCellValue();
                //cellInfo.setBottomCellValue();
                //cellInfo.setLeftCellValue();
                //cellInfo.setRightCellValue();
                cellInfoList.add(cellInfo);
                map.put(cellValue, cellInfoList);
            }
        }
        return map;
    }
}
