package com.uiparse.pdfparser.db;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by md_al on 01-Jan-19.
 */
@Entity
@Table(name = "training_file_set")
public class TrainingFileSet {

    @Id
    Integer id;

    @Column(name = "uploaded_by")
    Integer uploadedBy;

    @Column(name = "upload_date")
    Date uploadDate;

    @Column(name = "parser_id")
    Integer parserId;

    @Transient
    User user;

    public TrainingFileSet() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(Integer uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Integer getParserId() {
        return parserId;
    }

    public void setParserId(Integer parserId) {
        this.parserId = parserId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
