package com.uiparse.pdfparser.db;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by md_al on 01-Jan-19.
 */
@Entity
@Table(name = "result_mapping")
public class ResultMapping {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    @Column(name = "row_index")
    Integer rowIndex;

    @Column(name = "col_index")
    Integer colIndex;

    @Column(name = "is_static")
    Integer isStatic;

    @Column(name = "main_file_row_index")
    Integer mainFileRowIndex;

    @Column(name = "main_file_col_index")
    Integer mainFileColIndex;

    @Column(name = "parser_id")
    Integer parserId;

    public ResultMapping() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    public Integer getColIndex() {
        return colIndex;
    }

    public void setColIndex(Integer colIndex) {
        this.colIndex = colIndex;
    }

    public Integer getIsStatic() {
        return isStatic;
    }

    public void setIsStatic(Integer isStatic) {
        this.isStatic = isStatic;
    }

    public Integer getMainFileRowIndex() {
        return mainFileRowIndex;
    }

    public void setMainFileRowIndex(Integer mainFileRowIndex) {
        this.mainFileRowIndex = mainFileRowIndex;
    }

    public Integer getMainFileColIndex() {
        return mainFileColIndex;
    }

    public void setMainFileColIndex(Integer mainFileColIndex) {
        this.mainFileColIndex = mainFileColIndex;
    }

    public Integer getParserId() {
        return parserId;
    }

    public void setParserId(Integer parserId) {
        this.parserId = parserId;
    }
}
