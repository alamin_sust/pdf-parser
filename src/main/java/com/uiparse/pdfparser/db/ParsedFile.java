package com.uiparse.pdfparser.db;

import javax.persistence.*;
import java.util.Date;
import java.util.Optional;

@Entity
@Table(name = "parsed_file")
public class ParsedFile {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    @Column(name = "uploaded_by")
    Integer uploadedBy;

    @Column(name = "upload_date")
    Date uploadDate;

    @Column(name = "best_parser_id")
    Integer bestParserId;

    @Column(name = "accuracy")
    Double accuracy;

    @Transient
    Parser bestParser;

    public ParsedFile() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(Integer uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Integer getBestParserId() {
        return bestParserId;
    }

    public void setBestParserId(Integer bestParserId) {
        this.bestParserId = bestParserId;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    public Parser getBestParser() {
        return bestParser;
    }

    public void setBestParser(Parser bestParser) {
        this.bestParser = bestParser;
    }
}
