package com.uiparse.pdfparser.db;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by md_al on 05-Jan-19.
 */
public class ExcelRow {
    private List<String> colList = new LinkedList<>();
    private List<Integer> mappedRowIndexOfMainFileList = new LinkedList<>();
    private List<Integer> mappedColIndexOfMainFileList = new LinkedList<>();
    private List<MetaData> metaDataList = new LinkedList<>();

    public ExcelRow(List<String> colList) {
        this.colList = colList;
    }

    public ExcelRow() {

    }

    public List<String> getColList() {
        return colList;
    }

    public void setColList(List<String> colList) {
        this.colList = colList;
    }

    public List<Integer> getMappedRowIndexOfMainFileList() {
        return mappedRowIndexOfMainFileList;
    }

    public void setMappedRowIndexOfMainFileList(List<Integer> mappedRowIndexOfMainFileList) {
        this.mappedRowIndexOfMainFileList = mappedRowIndexOfMainFileList;
    }

    public List<Integer> getMappedColIndexOfMainFileList() {
        return mappedColIndexOfMainFileList;
    }

    public void setMappedColIndexOfMainFileList(List<Integer> mappedColIndexOfMainFileList) {
        this.mappedColIndexOfMainFileList = mappedColIndexOfMainFileList;
    }

    public List<MetaData> getMetaDataList() {
        return metaDataList;
    }

    public void setMetaDataList(List<MetaData> metaDataList) {
        this.metaDataList = metaDataList;
    }
}
