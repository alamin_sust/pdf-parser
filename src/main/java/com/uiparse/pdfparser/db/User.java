package com.uiparse.pdfparser.db;

import javax.persistence.*;

/**
 * Created by md_al on 12-Jan-19.
 */
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    Integer id;

    @Column(name = "email")
    String email;

    @Column(name = "password")
    String password;

    public User() {

    }

    public User(Integer id, String email, String password) {
        super();
        this.id = id;
        this.email = email;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
