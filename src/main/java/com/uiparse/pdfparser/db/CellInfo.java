package com.uiparse.pdfparser.db;

public class CellInfo {
    int rowIndex;
    int colIndex;
    String cellValue;
    String cellLabel;
    String topCellValue;
    String bottomCellValue;
    String leftCellValue;
    String rightCellValue;
    String topLeftCellValue;
    String topRightCellValue;
    String bottomLeftCellValue;
    String bottomRightCellValue;
    boolean isTopCellKey;
    boolean isLeftCellKey;
    boolean isStatic;
    int wordCount;
    String keyDirection;

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getColIndex() {
        return colIndex;
    }

    public void setColIndex(int colIndex) {
        this.colIndex = colIndex;
    }

    public String getCellValue() {
        return cellValue;
    }

    public void setCellValue(String cellValue) {
        this.cellValue = cellValue;
    }

    public String getCellLabel() {
        return cellLabel;
    }

    public void setCellLabel(String cellLabel) {
        this.cellLabel = cellLabel;
    }

    public String getTopCellValue() {
        return topCellValue;
    }

    public void setTopCellValue(String topCellValue) {
        this.topCellValue = topCellValue;
    }

    public String getBottomCellValue() {
        return bottomCellValue;
    }

    public void setBottomCellValue(String bottomCellValue) {
        this.bottomCellValue = bottomCellValue;
    }

    public String getLeftCellValue() {
        return leftCellValue;
    }

    public void setLeftCellValue(String leftCellValue) {
        this.leftCellValue = leftCellValue;
    }

    public String getRightCellValue() {
        return rightCellValue;
    }

    public void setRightCellValue(String rightCellValue) {
        this.rightCellValue = rightCellValue;
    }

    public String getTopLeftCellValue() {
        return topLeftCellValue;
    }

    public void setTopLeftCellValue(String topLeftCellValue) {
        this.topLeftCellValue = topLeftCellValue;
    }

    public String getTopRightCellValue() {
        return topRightCellValue;
    }

    public void setTopRightCellValue(String topRightCellValue) {
        this.topRightCellValue = topRightCellValue;
    }

    public String getBottomLeftCellValue() {
        return bottomLeftCellValue;
    }

    public void setBottomLeftCellValue(String bottomLeftCellValue) {
        this.bottomLeftCellValue = bottomLeftCellValue;
    }

    public String getBottomRightCellValue() {
        return bottomRightCellValue;
    }

    public void setBottomRightCellValue(String bottomRightCellValue) {
        this.bottomRightCellValue = bottomRightCellValue;
    }

    public boolean isTopCellKey() {
        return isTopCellKey;
    }

    public void setTopCellKey(boolean topCellKey) {
        isTopCellKey = topCellKey;
    }

    public boolean isLeftCellKey() {
        return isLeftCellKey;
    }

    public void setLeftCellKey(boolean leftCellKey) {
        isLeftCellKey = leftCellKey;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    public int getWordCount() {
        return wordCount;
    }

    public void setWordCount(int wordCount) {
        this.wordCount = wordCount;
    }

    public String getKeyDirection() {
        return keyDirection;
    }

    public void setKeyDirection(String keyDirection) {
        this.keyDirection = keyDirection;
    }
}
