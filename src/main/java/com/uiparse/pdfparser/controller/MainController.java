package com.uiparse.pdfparser.controller;

import com.uiparse.pdfparser.dao.*;
import com.uiparse.pdfparser.db.*;
import com.uiparse.pdfparser.service.AuthService;
import com.uiparse.pdfparser.service.FileService;
import com.uiparse.pdfparser.service.HelperService;
import com.uiparse.pdfparser.service.MainService;
import com.uiparse.pdfparser.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.*;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by md_al on 01-Jan-19.
 */
@Controller
public class MainController {

    @Autowired
    private MainService mainService;

    @Autowired
    private FileService fileService;

    @Autowired
    private AuthService authService;

    @Autowired
    private HelperService helperService;

    @Autowired
    private CrudDao crudDao;

    @Autowired
    private ParserDao parserDao;

    @Autowired
    private ParsedFileDao parsedFileDao;

    @Autowired
    private ResultMappingDao resultMappingDao;

    @Autowired
    private AuthDao authDao;

    @Autowired
    private TrainingFileSetDao trainingFileSetDao;

    @RequestMapping("/")
    public String root(Model model) {
        return "redirect:/parsers";
    }

    @RequestMapping("/home")
    public String home(Model model) {
        return "home";
    }

    @RequestMapping("/dragAndDropTest")
    public String dnd(Model model) {
        return "dragAndDropTest";
    }

    @RequestMapping(value = "/dragAndDropTest", method = RequestMethod.POST)
    public String dndPost(Model model) {
        return "dragAndDropTest";
    }

    @RequestMapping("/parsers")
    public String parsers(Model model, HttpServletResponse response, HttpSession session, @RequestParam(name = "parserName", required = false) String parserName,
                          @RequestParam(name = "parserId", required = false) Integer parserId,
                          @RequestParam(name = "deleteParserId", required = false) String deleteParserId) throws IOException {

        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }

        if(deleteParserId!=null) {
            parserDao.deleteById(Integer.valueOf(deleteParserId));
            parsedFileDao.deleteByBestParserId(Integer.valueOf(deleteParserId));
            resultMappingDao.deleteByParserId(Integer.valueOf(deleteParserId));
            trainingFileSetDao.deleteByParserId(Integer.valueOf(deleteParserId));
            model.addAttribute("parserList", parserDao.findAllByCreatedBy(Integer.valueOf(session.getAttribute("id").toString())));
            session.setAttribute("successMessage", "Deleted Successfully!");
            return "parsers";
        }

        //mainService.parseTextFromPdfAndWriteInExcel();
        //model.addAttribute("pdf", mainService.getPdf(1));
        if(parserName!=null && parserId != null) {
            Parser parser = new Parser();
            parser.setId(parserId);
            parser.setName(parserName);
            parser.setCreatedBy(Integer.valueOf(session.getAttribute("id").toString()));
            parser.setCreateDate(new Date());
            parserDao.save(parser);
            //session.setAttribute("successMessage", "Successfully Inserted");
            response.sendRedirect("parsers");
        }

        model.addAttribute("parserList", parserDao.findAllByCreatedBy(Integer.valueOf(session.getAttribute("id").toString())));
        return "parsers";
    }

    @RequestMapping("/createParserTestParse")
    public String createParserTestParse(Model model,
                                        HttpSession session,
                                        @RequestParam("parserId") String parserId) throws IOException, SQLException {

        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }

        //mainService.train(parserId, Integer.parseInt(session.getAttribute("id").toString()));

        model.addAttribute("newParserId", parserId);
        model.addAttribute("parser", parserDao.findOneById(Integer.valueOf(parserId)));

        return "createParserTestParse";
    }

    @RequestMapping("/createParserUploadFiles")
    public String createParserUploadFiles(Model model, HttpSession session,
                                          @RequestParam(name = "parserId", required = false) String parserId,
                                          @RequestParam(name = "deleteTrainingId", required = false) String deleteTrainingId)
    throws SQLException {

        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }

        if(deleteTrainingId!=null) {
            trainingFileSetDao.deleteById(Integer.valueOf(deleteTrainingId));
            session.setAttribute("successMessage", "Deleted Successfully!");
        }

        setReferenceDataOfCreateParserUploadFiles(model,parserId==null?crudDao.getNextParserId():parserId, Integer.parseInt(session.getAttribute("id").toString()));


        return "createParserUploadFiles";
    }

    @RequestMapping("/login")
    public String login(Model model, HttpServletRequest request, HttpServletResponse response,
                        HttpSession session,
                        @RequestParam(name = "type", required = false) String type) {
        if(type != null && type.equals("Logout")) {
            authService.logout(request, session);
        }
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginPost(Model model, HttpServletRequest request,
                            @RequestParam("type") String type,
                            HttpServletResponse response,
                            HttpSession session) throws IOException {
        if(type.equals("Login") && authService.login(request, session)) {
            response.sendRedirect("parsers");
        } else if(type.equals("Register")) {
            authService.register(request, session);
        }
        return "login";
    }

    @RequestMapping("/uploadFiles")
    public String uploadFiles(Model model, HttpSession session)
    {
        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }
        return "uploadFiles";
    }

    @RequestMapping(value = "/createParserUploadFiles", method = RequestMethod.POST)
    public String createParserUploadFiles(//@RequestParam("fileName") String fileName,
                            HttpSession session,
                            Model model,
                            @RequestParam("parserId") String parserId,
                            @RequestPart("pdfFile") MultipartFile pdfFile,
                            @RequestPart("excelFile") MultipartFile excelFile,
                            @RequestPart(value = "excelMetaDataFile", required = false) MultipartFile excelMetaDataFile) {

        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }

        if (helperService.isFileTypeSame(pdfFile,"pdf") && helperService.isFileTypeSame(excelFile,"xls")) {
            Parser parser = new Parser();
            parser.setId(Integer.valueOf(parserId));
            parser.setCreatedBy(Integer.valueOf(session.getAttribute("id").toString()));
            parser.setCreateDate(new Date());
            parserDao.save(parser);
            fileService.processTrainingFileSet(pdfFile, excelFile, excelMetaDataFile, session, parserId);
        } else {
            session.setAttribute("errorMessage", "Please upload files in .pdf and .xls format only");
        }

        setReferenceDataOfCreateParserUploadFiles(model,parserId, Integer.parseInt(session.getAttribute("id").toString()));

        return "createParserUploadFiles";
    }

    private void setReferenceDataOfCreateParserUploadFiles(Model model, String parserId, int uploadedBy) {
        List<TrainingFileSet> trainingFileSetList = trainingFileSetDao.findAllByParserIdAndUploadedBy(Integer.parseInt(parserId), uploadedBy);

        for (TrainingFileSet trainingFileSet : trainingFileSetList) {
            User user = authDao.findOneById(trainingFileSet.getUploadedBy());
            trainingFileSet.setUser(user);
        }

        model.addAttribute("trainingFileSetList", trainingFileSetList);
        model.addAttribute("newParserId",parserId);
        model.addAttribute("parser", parserDao.findOneById(Integer.valueOf(parserId)));
    }

    @RequestMapping(value = "/createParserTestParse", method = RequestMethod.POST)
    public String createParserTestParse(HttpSession session,
                                          Model model,
                                          HttpServletResponse response,
                                          HttpServletRequest request,
                                          @RequestParam("parserId") String parserId,
                                          @RequestPart("pdfFile") MultipartFile pdfFile) throws IOException, SQLException {

        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }

        if (helperService.isFileTypeSame(pdfFile,"pdf")) {
            fileService.processTestingFile(pdfFile, session, parserId);

            List<TrainingFileSet> trainingFileSetList = trainingFileSetDao.findAllByParserIdAndUploadedBy(Integer.parseInt(parserId), Integer.parseInt(session.getAttribute("id").toString()));

            double accuracy = mainService.trainAndTest2(parserId, Integer.parseInt(session.getAttribute("id").toString()), trainingFileSetList.get(0).getUploadedBy()+"_"+trainingFileSetList.get(0).getParserId() ,Constants.TESTING_FILE_PATH, String.valueOf(parserId), Constants.TESTING_RESULT_FILE_PATH);
            model.addAttribute("accuracy", Math.round(accuracy*100)/100.0);
            model.addAttribute("showFileRow",true);
        } else {
            session.setAttribute("errorMessage", "Please upload files in .pdf format only");
        }

        model.addAttribute("newParserId",parserId);
        model.addAttribute("parser", parserDao.findOneById(Integer.valueOf(parserId)));

        return "createParserTestParse";
    }

    @RequestMapping(value = "/uploadFiles", method = RequestMethod.POST)
    public String uploadFilesPost(HttpSession session,
                                        Model model,
                                        HttpServletResponse response,
                                  HttpServletRequest request,
                                  @RequestPart("files") MultipartFile[] files) throws IOException, SQLException, ServletException {

        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }

        for(String fileName: ((StandardMultipartHttpServletRequest) request).getMultiFileMap().keySet()){
            if ("pdf".equals(fileName.split("\\.")[fileName.split("\\.").length-1])) {
                fileService.processTestingFileAndDetermineParser(((StandardMultipartHttpServletRequest) request).getMultiFileMap().toSingleValueMap().get(fileName),session);
            } else {
                session.setAttribute("errorMessage", "Please upload files in .pdf format only");
            }
        }

        return "uploadFiles";
    }

    @RequestMapping(value = "/downloadFile")
    public String downloadFile(HttpServletResponse response, Model model,
                                @RequestParam(value = "userId", required = false) String userId,
                                @RequestParam(value = "parserId", required = false) String parserId,
                                @RequestParam(value = "dbId", required = false) String dbId,
                                @RequestParam("fileExtension") String fileExtension,
                                @RequestParam("fileType") String fileType,HttpSession session) {

        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }

        if (fileType.equals("resultPdf")) {
            fileService.downloadFile(response, Constants.TESTING_PARSER_AUTO_DETERMINATION_FILE_PATH , dbId + fileExtension);
        } else if (fileType.equals("resultXls")){
            fileService.downloadFile(response, Constants.TESTING_PARSER_AUTO_DETERMINATION_RESULT_FILE_PATH , dbId + fileExtension);
        } else if (fileType.equals("sampleMetadata")) {
            fileService.downloadFile(response, Constants.SAMPLE_META_DATA_FILE_PATH , "sample_meta_data.xls");
        }

        String rootPath = fileType.equals("testResult") ? Constants.TESTING_RESULT_FILE_PATH :
                (fileType.equals("train") ? Constants.TRAINING_FILE_PATH :
                        (fileType.equals("metadata") ? Constants.TRAINING_FILE_PATH: Constants.TESTING_FILE_PATH ));

        String fileName = "";

        if (userId != null) {
            fileName += userId+"_";
        }

        fileName += parserId;

        if (dbId != null && !fileType.equals("metadata")) {
            fileName += "_" + dbId;
        } else if (fileType.equals("metadata")) {
            fileName += "_" + "metadata";
        }

        fileService.downloadFile(response, rootPath , fileName + fileExtension);
        model.addAttribute("newParserId",parserId);
        model.addAttribute("parser", parserDao.findOneById(Integer.valueOf(parserId)));
        return "redirect:/createParserTestParse";
    }

    @RequestMapping(value = "/train")
    public String train(HttpSession session) throws IOException {
        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }

        List<ExcelRow> rowList = new LinkedList<>();
        rowList.add(new ExcelRow(Arrays.asList("Header 1", "Header 2")));
        rowList.add(new ExcelRow(Arrays.asList("Value 1", "Value 2")));

        //fileService.writeExcelContent(rowList, "2f.xlsx");

        session.setAttribute("successMessage", "Train Successful!");

        return "redirect:createParserTestParse";
    }

    @RequestMapping("/parsedFiles")
    public String parsedFiles(Model model, HttpServletResponse response, HttpSession session) throws IOException {

        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }

        List<ParsedFile> parsedFileList = parsedFileDao.findAllByUploadedByAndMinAccuracy(Integer.valueOf(session.getAttribute("id").toString()), 60.0);
        for(ParsedFile parsedFile:parsedFileList) {
            parsedFile.setBestParser(parserDao.findOneById(parsedFile.getBestParserId()));
            parsedFile.setAccuracy(Math.round(parsedFile.getAccuracy()*100)/100.0);
        }

        model.addAttribute("parsedFileList", parsedFileList);
        return "parsedFiles";
    }

    @RequestMapping("/unparsedFiles")
    public String unparsedFiles(Model model, HttpServletResponse response, HttpSession session) throws IOException {
        if (session.getAttribute("id") == null) {
            helperService.updateSession(session, "errorMessage", "Please Login!");
            return "redirect:/login";
        }

        List<ParsedFile> parsedFileList = parsedFileDao.findAllByUploadedByAndMaxAccuracy(Integer.valueOf(session.getAttribute("id").toString()), 60.0);
        for(ParsedFile parsedFile:parsedFileList) {
            parsedFile.setBestParser(parserDao.findOneById(parsedFile.getBestParserId()));
        }

        model.addAttribute("parsedFileList", parsedFileList);
        return "unparsedFiles";
    }

}
