<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-TileColor" content="#0061da">
    <meta name="theme-color" content="#1643a3">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

    <!-- Title -->
    <title>SparkyAi-Login</title>

    <!--Font Awesome-->
    <link href="resources/assets/plugins/fontawesome-free/css/all.css" rel="stylesheet">

    <!-- Font Family -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">

    <!-- Custom scroll bar css-->
    <link href="resources/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

    <!-- Dashboard Css -->
    <link href="resources/assets/css/dashboard.css" rel="stylesheet" />

    <!-- c3.js Charts Plugin -->
    <link href="resources/assets/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

    <!---Font icons-->
    <link href="resources/assets/plugins/iconfonts/plugin.css" rel="stylesheet" />

</head>
<body class="login-img bg-gradient">
<!-- Header Background Animation-->
<div id="particles-js"  class=""></div>
<div id="global-loader" ><div class="showbox"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div></div>
<div class="page">
    <div class="page-single">
        <div class="container">
            <%--<jsp:include page="includes/successAndErrorMessages.jsp"/>--%>
            <div class="row">
                <div class="col col-login mx-auto">
                    <div class="text-center mb-6 ">
                        <img alt="ren logo" class="header-brand-img" src="resources/assets/bw-logo.png" style="height: 30px !important; padding-bottom: 5px;">
                    </div>
                    <jsp:include page="includes/successAndErrorMessages.jsp"/>
                    <form class="card" action="login" method="post">
                        <div class="card-body p-6">
                            <div class="card-title text-center">Login/Register</div>
                            <div class="form-group">
                                <label class="form-label">Email address</label>
                                <input type="email" name="email" class="form-control" id="exampleInputEmail1"  placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Password
                                    <%--<a href="./forgot-password.html" class="float-right small">I forgot password</a>--%>
                                </label>
                                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" name="rememberMe" class="custom-control-input" />
                                    <span class="custom-control-label">Remember me</span>
                                </label>
                            </div>
                            <div class="form-footer">
                                <%if(session.getAttribute("email")!=null && session.getAttribute("email").toString().equals("admin@sparkyai.com")){%>
                                <input type="submit" name="type" value="Register" class="btn btn-primary btn-block"/>
                                <%} else {%>
                                <input type="submit" name="type" value="Login" class="btn btn-primary btn-block"/>
                                <%}%>
                                <input type="button" value="Return to Home" class="btn btn-default btn-block" onclick="location.href='parsers'"/>
                            </div>
                            <%--<div class="text-center text-muted mt-3">
                                Don't have account yet? <a href="#">Sign up</a>
                            </div>--%>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Dashboard js -->
<script src="resources/assets/js/vendors/jquery-3.2.1.min.js"></script>
<script src="resources/assets/js/vendors/bootstrap.bundle.min.js"></script>
<script src="resources/assets/js/vendors/jquery.sparkline.min.js"></script>
<script src="resources/assets/js/vendors/selectize.min.js"></script>
<script src="resources/assets/js/vendors/jquery.tablesorter.min.js"></script>
<script src="resources/assets/js/vendors/circle-progress.min.js"></script>
<script src="resources/assets/plugins/rating/jquery.rating-stars.js"></script>

<!-- Custom scroll bar Js-->
<script src="resources/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- animation -->
<script src="resources/assets/plugins/particles/particles.js"></script>
<script src="resources/assets/plugins/particles/particlesapp_default.js"></script>

<!--Counters -->
<script src="resources/assets/plugins/counters/counterup.min.js"></script>
<script src="resources/assets/plugins/counters/waypoints.min.js"></script>

<!-- custom js -->
<script src="resources/assets/js/custom.js"></script>
<script>
    var colors = new Array(
        [94,114,228],
        [130,94,228],
        [45,206,137],
        [45,206,204],
        [17,205,239],
        [17,113,239],
        [245,54,92],
        [245,96,54]);
</script>
</body>
</html>
