<!doctype html>
<html lang="en" dir="ltr">
<head>

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="msapplication-TileColor" content="#0061da">
    <meta name="theme-color" content="#1643a3">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="favicon4x.png" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="favicon4x.png"/>

    <!-- Title -->
    <title>SparkyAi - Upload Reference Files</title>

    <!--Font Awesome-->
    <link href="resources/assets/plugins/fontawesome-free/css/all.css" rel="stylesheet">

    <!-- Font Family -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600" rel="stylesheet">

    <!-- Dashboard Css -->
    <link href="resources/assets/css/dashboard.css" rel="stylesheet" />

    <!-- Custom scroll bar css-->
    <link href="resources/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

    <!-- Sidemenu Css -->
    <link href="resources/assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

    <!-- c3.js Charts Plugin -->
    <link href="resources/assets/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

    <!---Font icons-->
    <link href="resources/assets/plugins/iconfonts/plugin.css" rel="stylesheet" />

</head>
<body class="app">
<div id="global-loader" ><div class="showbox"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div></div>
<div class="page">
    <div class="page-main">
        <div class="container">
            <div class="side-app">
                <div class="page-header">
                    <h4 class="page-title">Create Parser</h4> <%--<input type="text" class="form-control col-3 bg-white" placeholder="Enter Parser Name" >--%>

                    <ol class="breadcrumb text-right">
                        <li class="text-right"><a href="parsers" class="btn btn-primary">Cancel & Exit</a>
                        </li>
                        <c:if test="${not empty trainingFileSetList}"><li class="ml-5 text-right"><a href="createParserTestParse?parserId=${newParserId}" class="btn btn-success">Save & Proceed <icon class="fa fa-caret-right"></i></a></li></c:if>
                    </ol>

                </div>
                <jsp:include page="includes/successAndErrorMessages.jsp"/>
                <!--Modal Start-->
                <!-- Modal End -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body text-center">
                                <button type="button" class="btn btn-blue m-l-15 m-r-10 m-b-10 disabled">Upload Training Sets<span class="badge badge-white"><icon class="fas fa-chevron-right"></icon></span></button>
                                <!-- 										<button type="button" class="btn btn-light ml-3 m-b-10 disabled">Set Extraction Targets<span class="badge badge-light"><icon class="fas fa-chevron-right"></icon></span></button>
                                    <button type="button" class="btn btn-light ml-3 m-b-10 disabled">Setup Csv output<span class="badge badge-white"><icon class="fas fa-chevron-right"></icon></span></button> -->
                                <button type="button" class="btn btn-light ml-3  m-b-10 disabled">Test Parse &amp; Finish</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="alert alert-info">
                        <strong>Tips:</strong> Upload minimum 3 training sets to ensure you get better outcome.
                        <a href="#" class="btn btn-xs btn-info float-right">GOT IT</a>
                    </div>
                </div>
                <div class="row row-cards">
                    <div class="col-lg-4">
                        <form class="card" method="POST" action="createParserUploadFiles" enctype="multipart/form-data">
                            <div class="card-header">
                                <div class="card-title">Upload Training set</div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="form-label">Upload pdf</div>
                                    <div class="custom-file">
                                        <input type="file" class="alert alert-primary" name="pdfFile" required>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <div class="form-label">Upload xls</div>
                                    <div class="custom-file">
                                        <input type="file" class="alert alert-primary" name="excelFile" required>
                                    </div>
                                </div>
                                <br>
                                <%--<c:if test="${empty trainingFileSetList}">--%>
                                    <div class="form-group">
                                        <div class="form-label"><a href="downloadFile?fileExtension=.xls&fileType=sampleMetadata"><i class="far fa-file-excel text-green ml-2"></i> sample meta data file</a></div>
                                        <div class="form-label"><b>Note: Only the latest uploaded meta data file will be used for training</b></div>
                                        <div class="form-label">Upload xls (Helping file)</div>
                                        <div class="custom-file">
                                            <input type="file" class="alert alert-primary" name="excelMetaDataFile">
                                        </div>
                                        <br>

                                    </div>
                                <%--</c:if>--%>
                                <div class="form-footer">
                                    <input type="hidden" name="parserId" value="${newParserId}"/>
                                    <button type="submit" class="btn btn-primary btn-block">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-8 mb-8">
                        <p>Displaying list of uploaded training sets</p>
                        <div class="card p-3">

                            <div class="table-responsive">
                                <table class="table card-table table-vcenter text-nowrap">
                                    <thead >
                                    <tr>
                                        <th>Ref File</th>
                                        <th>Training Set</th>
                                        <th>File Name</th>
                                        <th>Created by</th>
                                        <th>Created On</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${trainingFileSetList}" var="trainingFileSet" varStatus="loop">
                                    <c:if test="${loop.index == 0}">
                                        <tr>
                                            <td scope="row" class="text-center"></td>
                                            <td class="text-center"><a href="downloadFile?userId=<%=session.getAttribute("id")%>&parserId=${newParserId}&fileExtension=.xls&fileType=metadata"><i class="far fa-file-excel text-green ml-2"></i></a></td>
                                            <td>${trainingFileSet.uploadedBy}_${trainingFileSet.parserId}_metadata</td>
                                            <td>${trainingFileSet.user.email}</td>
                                            <td>${trainingFileSet.uploadDate}</td>
                                            <td><b>To update this meta data file, please upload the file again</b></td>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <td scope="row" class="text-center"><a href="downloadFile?userId=<%=session.getAttribute("id")%>&parserId=${newParserId}&dbId=${trainingFileSet.id}&fileExtension=.pdf&fileType=train"><i class="far fa-file-pdf text-red"></i></a>

                                        </td>
                                        <td class="text-center"><a href="downloadFile?userId=<%=session.getAttribute("id")%>&parserId=${newParserId}&dbId=${trainingFileSet.id}&fileExtension=.xls&fileType=train"><i class="far fa-file-excel text-green ml-2"></i></a></td>
                                        <td>${trainingFileSet.uploadedBy}_${trainingFileSet.parserId}_${trainingFileSet.id}</td>
                                        <td>${trainingFileSet.user.email}</td>
                                        <td>${trainingFileSet.uploadDate}</td>
                                        <td><a href="createParserUploadFiles?deleteTrainingId=${trainingFileSet.id}&parserId=${newParserId}"><i class="far fa-trash-alt primary text-red"></i></a></td>
                                    </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--footer-->
        <footer class="footer fixed-bottom">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
                        Copyright &#169; 2018 <a href="#">sparkyai</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer-->
    </div>
</div>

<!-- Back to top -->
<a href="#top" id="back-to-top" style="display: inline;"><i class="fas fa-angle-up"></i></a>

<!-- Dashboard js -->
<script src="resources/assets/js/vendors/jquery-3.2.1.min.js"></script>
<script src="resources/assets/js/vendors/bootstrap.bundle.min.js"></script>
<script src="resources/assets/js/vendors/jquery.sparkline.min.js"></script>
<script src="resources/assets/js/vendors/selectize.min.js"></script>
<script src="resources/assets/js/vendors/jquery.tablesorter.min.js"></script>
<script src="resources/assets/js/vendors/circle-progress.min.js"></script>
<script src="resources/assets/plugins/rating/jquery.rating-stars.js"></script>
<!-- Side menu js -->
<script src="resources/assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

<!-- Custom scroll bar Js-->
<script src="resources/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

<!--Counters -->
<script src="resources/assets/plugins/counters/counterup.min.js"></script>
<script src="resources/assets/plugins/counters/waypoints.min.js"></script>

<!-- custom js -->
<script src="resources/assets/js/custom.js"></script>

</body>
</html>