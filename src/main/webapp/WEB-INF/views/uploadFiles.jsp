<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="msapplication-TileColor" content="#0061da">
    <meta name="theme-color" content="#1643a3">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="resources/assets/favicon4x.png" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="resources/assets/favicon4x.png"/>

    <!-- Title -->
    <title>SparkyAi - Parsed Files</title>

    <!--Font Awesome-->
    <link href="resources/assets/plugins/fontawesome-free/css/all.css" rel="stylesheet">

    <!-- Font Family -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600" rel="stylesheet">

    <!-- Dashboard Css -->
    <link href="resources/assets/css/dashboard.css" rel="stylesheet" />

    <!-- Custom scroll bar css-->
    <link href="resources/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

    <!-- Sidemenu Css -->
    <link href="resources/assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

    <!-- c3.js Charts Plugin -->
    <link href="resources/assets/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

    <!---Font icons-->
    <link href="resources/assets/plugins/iconfonts/plugin.css" rel="stylesheet" />

    <link type="text/css" rel="stylesheet" href="resources/assets/css/drag-and-drop.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="resources/assets/js/drag-and-drop.js"></script>
    <script type="text/javascript">
        var afterUploaded = function(resp) {
            alert("Successfully Uploaded!");
        };

        $(document).ready(function() {
            $('input[type="file"]').dropUpload({
                'uploadUrl'	: '/uploadFiles',
                'uploaded'	: afterUploaded,
                'dropClass' 	: 'file-drop',
                'dropHoverClass': 'file-drop-hover',
                'defaultText'  	: 'Drop your files here!',
                'hoverText'	: 'Let go to upload!'
            });
        });
    </script>

</head>
<body class="app">
<div id="global-loader" ><div class="showbox"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div></div>
<div class="page">
    <div class="page-main">
        <jsp:include page="includes/header.jsp"/>
        <jsp:include page="includes/successAndErrorMessages.jsp"/>
        <!-- Horizantal menu-->
        <div class="ren-navbar" id="headerMenuCollapse">
            <div class="container">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="parsers">
                            <i class="fas fa-crosshairs"></i>
                            <span>Parsers</span>
                        </a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link " href="#">
                            <i class="fas fa-file-upload"></i>
                            <span>Upload Files</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="parsedFiles">
                            <i class="fe fe-file-text" data-toggle="tooltip" title="" data-original-title="fe fe-file-text"></i>
                            <span>Parsed Files</span>
                        </a>
                    </li>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="unparsedFiles">
                            <i class="fe fe-alert-octagon" data-toggle="tooltip" title="" data-original-title="fe fe-alert-octagon"></i>
                            <span>Unparsed</span>
                            <span class="square-8"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Horizantal menu-->

        <div class="container">
            <div class="side-app">
                <div class="page-header">
                    <h4 class="page-title">Upload Files</h4>
                </div>
                <div class="row row-cards">
                    <div class="col-lg-12 mb-8">

                        <div class="card p-3 file-drop">
                            <p>Upload new files in pdf format. Your parser will auto detect & parse them. Please check for the output in parsed or unarsed pages</p>
                            <form action="uploadFiles" method="post" enctype="multipart/form-data">
                                <input style="display:none;" type="file" name="files[]" multiple="multiple">
                                <input style="display:none;" type="submit" value="Upload!">
                            </form>
                        </div>
                    </div>
                </div>

                <!-- table-responsive -->
            </div>
        </div>
    </div>
</div>
</div>
<!--footer-->
<footer class="footer fixed-bottom">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
                Copyright &#169; 2018 <a href="#">sparkyai</a>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer-->
</div>
</div>

<!-- Back to top -->
<a href="#top" id="back-to-top" style="display: inline;"><i class="fas fa-angle-up"></i></a>

<!-- Dashboard js -->
<script src="resources/assets/js/vendors/bootstrap.bundle.min.js"></script>
<script src="resources/assets/js/vendors/jquery.sparkline.min.js"></script>
<script src="resources/assets/js/vendors/selectize.min.js"></script>
<script src="resources/assets/js/vendors/jquery.tablesorter.min.js"></script>
<script src="resources/assets/js/vendors/circle-progress.min.js"></script>
<script src="resources/assets/plugins/rating/jquery.rating-stars.js"></script>
<!-- Side menu js -->
<script src="resources/assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

<!-- Custom scroll bar Js-->
<script src="resources/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

<!--Counters -->
<script src="resources/assets/plugins/counters/counterup.min.js"></script>
<script src="resources/assets/plugins/counters/waypoints.min.js"></script>

<!-- custom js -->
<script src="resources/assets/js/custom.js"></script>

</body>
</html>