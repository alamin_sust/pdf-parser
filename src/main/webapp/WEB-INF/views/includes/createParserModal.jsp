<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="GET" action="parsers">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Parser</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Parser Name: <input class="form-control bg-white" type="text" name="parserName" placeholder="Enter Parser Name" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>



                    <input type="hidden" name="parserId" value="${newParserId}"/>
                    <button type="submit" class="btn btn-primary">Confirm</button>

            </div>
            </form>
        </div>
    </div>
</div>