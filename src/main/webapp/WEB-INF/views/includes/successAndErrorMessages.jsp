<%if(session.getAttribute("successMessage") != null) {%>
<div class="alert alert-success">
    <%=session.getAttribute("successMessage")%>
</div>
<%
session.setAttribute("successMessage", null);
}%>

<%if(session.getAttribute("errorMessage") != null) {%>
<div class="alert alert-danger">
    <%=session.getAttribute("errorMessage")%>
</div>
<%
session.setAttribute("errorMessage", null);
}%>