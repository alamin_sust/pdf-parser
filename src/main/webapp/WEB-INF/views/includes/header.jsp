<!-- Navbar-->
<header class="app-header header shadow-none relative">
    <div class="container">

        <!-- Navbar Right Menu-->
        <div class="container-fluid">
            <div class="d-flex">
                <a class="header-brand" href="parsers">
                    <img alt="ren logo" class="header-brand-img" src="resources/assets/logo.png" style="height: 20px !important; padding-bottom: 5px;">
                </a>
                <%if(session.getAttribute("email")!=null){%>
                <div class="d-flex order-lg-2 ml-auto">
                    <div class="dropdown">
                        <a class="nav-link pr-0 leading-none d-flex" data-toggle="dropdown" href="#">
                            <span class="avatar avatar-sm brround" style="background-image: url(resources/assets/images/faces/female/25.jpg)"></span>
                            <span class="ml-2 d-none d-lg-block">
                                                    <span><%=session.getAttribute("email")%></span>
                                                </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <%if(session.getAttribute("email")!=null && session.getAttribute("email").toString().equals("admin@sparkyai.com")){%>
                            <a class="dropdown-item" href="login"><i class="dropdown-icon mdi mdi-logout-variant"></i> Register New User </a>
                            <%}%>

                            <a class="dropdown-item" href="login?type=Logout"><i class="dropdown-icon mdi mdi-logout-variant"></i> Logout </a>
                        </div>
                    </div>
                </div>
                <%} else {%>
                <div class="d-flex order-lg-2 ml-auto">
                    <div class="dropdown">
                        <a class="nav-link pr-0 leading-none d-flex" href="login">
                            <span class="avatar avatar-sm brround" style="background-image: url(resources/assets/images/faces/female/25.jpg)"></span>
                            <span class="ml-2 d-none d-lg-block">
                                                    <span>login</span>
                                                </span>
                        </a>
                    </div>
                </div>
                <%}%>
                <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                    <span class="header-toggler-icon"></span>
                </a>
            </div>
        </div>
    </div>
</header>