<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="msapplication-TileColor" content="#0061da">
    <meta name="theme-color" content="#1643a3">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href=resources/assets/favicon4x.png" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href=resources/assets/favicon4x.png"/>

    <!-- Title -->
    <title>SparkyAi - Parsed Files</title>

    <!--Font Awesome-->
    <link href="resources/assets/plugins/fontawesome-free/css/all.css" rel="stylesheet">

    <!-- Font Family -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600" rel="stylesheet">

    <!-- Dashboard Css -->
    <link href="resources/assets/css/dashboard.css" rel="stylesheet" />

    <!-- Custom scroll bar css-->
    <link href="resources/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

    <!-- Sidemenu Css -->
    <link href="resources/assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

    <!-- c3.js Charts Plugin -->
    <link href="resources/assets/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

    <!---Font icons-->
    <link href="resources/assets/plugins/iconfonts/plugin.css" rel="stylesheet" />

</head>
<body class="app">
<div id="global-loader" ><div class="showbox"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div></div>
<div class="page">
    <div class="page-main">
        <!-- Navbar-->
        <!-- 			<header class="app-header header shadow-none relative">
                        <div class="container">

                            <!-- Navbar Right Menu-->
        <!-- 			<div class="container-fluid">
                        <div class="d-flex">
                            <a class="header-brand" href="parsers">
                                <img alt="ren logo" class="header-brand-img" src="ui-parse-logo4x.png" style="height: 20px !important; padding-bottom: 5px;">
                            </a>
                            <div class="d-flex order-lg-2 ml-auto">
                                <div class="dropdown">
                                    <a class="nav-link pr-0 leading-none d-flex" data-toggle="dropdown" href="#">
                                        <span class="avatar avatar-sm brround" style="background-image: url(resources/assets/images/faces/female/25.jpg)"></span>
                                        <span class="ml-2 d-none d-lg-block">
                                            <span>Sindy Scribner</span>
                                        </span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="login"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                                <span class="header-toggler-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </header>  -->

        <!-- Horizantal menu-->
        <!-- 				<div class="ren-navbar" id="headerMenuCollapse">
                            <div class="container">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#">
                                            <i class="fas fa-crosshairs"></i>
                                            <span>Parsers</span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link " href="#">
                                            <i class="fas fa-file-upload"></i>
                                            <span>Upload Files</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="parsedFiles">
        <i class="fe fe-file-text" data-toggle="tooltip" title="" data-original-title="fe fe-file-text"></i>
                                            <span>Parsed Files</span>
                                        </a>
                                    </li>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="unparsedFiles">
        <i class="fe fe-alert-octagon" data-toggle="tooltip" title="" data-original-title="fe fe-alert-octagon"></i>
                                            <span>Unparsed</span>
                                            <span class="square-8"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
        <!-- Horizantal menu-->

        <div class="container">
            <div class="side-app">
                <div class="page-header">
                    <h4 class="page-title">Create Parser</h4> <input type="text" class="form-control col-3 bg-white" placeholder="Enter Parser Name" value="Telstra-Consolidated">

                    <ol class="breadcrumb text-right">
                        <li class="text-right"><a href="parsers" class="btn btn-danger">Cancel & Exit</a></li>
                        <li class="text-right ml-5"><a href="createParserUploadFiles" class="btn btn-blue"> <i class="fas fa-caret-left"></i> Previous Step </a></li>
                        <li class="ml-5 text-right"><a href="createParserSetupCsv" class="btn btn-success">Save & Proceed <icon class="fa fa-caret-right"></i></a></li>
                    </ol>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body text-center">
                                <button type="button" class="btn btn-light m-l-15 m-r-10 m-b-10 disabled">Upload Reference PDF  <span class="badge badge-white"><icon class="fas fa-check-circle text-success"></icon></span></button>
                                <button type="button" class="btn btn-primary ml-3 m-b-10 disabled">Set Extraction Targets <span class="badge badge-light"><icon class="fas fa-chevron-right"></icon></span></button>
                                <button type="button" class="btn btn-light ml-3 m-b-10 disabled" >Setup Csv output<span class="badge badge-white"><icon class="fas fa-chevron-right"></icon></span></button>
                                <button type="button" class="btn btn-light ml-3  m-b-10 disabled">Test Parse &amp; Finish</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row-cards">
                    <div class="col-lg-2">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">

                                <div class="card item-card">
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <a><icon class="fas fa-times text-left"></i></a>
                                            <img src="resources/assets/images/products/1.png" alt="img" class="">
                                            <div class="text-center">1</div></div>
                                        <div class="mb-3">
                                            <a><icon class="fas fa-times text-left"></i></a>
                                            <img src="resources/assets/images/products/1.png" alt="img" class="">
                                            <div class="text-center">2</div></div>
                                        <div class="mb-3">
                                            <a><icon class="fas fa-times text-left"></i></a>
                                            <img src="resources/assets/images/products/1.png" alt="img" class="">
                                            <div class="text-center">3</div></div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10 mb-8">

                        <div class="card p-3">
                            <p>Click and drag your mouse to select the areas you wish to extract data</p>
                            <img src="resources/assets/images/products/14.png" alt="" class="" style="height:1600px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--footer-->
        <footer class="footer fixed-bottom">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
                        Copyright &#169; 2018 <a href="#">sparkyai</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer-->
    </div>
</div>

<!-- Back to top -->
<a href="#top" id="back-to-top" style="display: inline;"><i class="fas fa-angle-up"></i></a>

<!-- Dashboard js -->
<script src="resources/assets/js/vendors/jquery-3.2.1.min.js"></script>
<script src="resources/assets/js/vendors/bootstrap.bundle.min.js"></script>
<script src="resources/assets/js/vendors/jquery.sparkline.min.js"></script>
<script src="resources/assets/js/vendors/selectize.min.js"></script>
<script src="resources/assets/js/vendors/jquery.tablesorter.min.js"></script>
<script src="resources/assets/js/vendors/circle-progress.min.js"></script>
<script src="resources/assets/plugins/rating/jquery.rating-stars.js"></script>
<!-- Side menu js -->
<script src="resources/assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

<!-- Custom scroll bar Js-->
<script src="resources/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

<!--Counters -->
<script src="resources/assets/plugins/counters/counterup.min.js"></script>
<script src="resources/assets/plugins/counters/waypoints.min.js"></script>

<!-- custom js -->
<script src="resources/assets/js/custom.js"></script>

</body>
</html>