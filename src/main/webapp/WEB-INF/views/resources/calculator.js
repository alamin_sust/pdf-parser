var isReg = 1;
var cDays = 26;
var lastBeginDate = '';
function update1(val) {
    isReg = val;
}
function update2(val) {
    cDays = val;
}
function calculate() {
    lastBeginDate = $('#lastBeginDate').val();
    var res = '';
    if (lastBeginDate != '') {
        if (isReg == 0) {
            res = 'This method is designed for those with a regular period ranging from 26-32 days only.';
        } else {
            var toAddDays = 6 + cDays - 26;
            var dateF = new Date(Date.parse(lastBeginDate + "T05:00:00+0000"));
            var date2F = new Date(lastBeginDate + "T05:00:00+0000");
            date2F.setDate(date2F.getDate() + 20);

            var dateT = new Date(lastBeginDate + "T05:00:00+0000");
            dateT.setDate(dateT.getDate() + 6);
            var date2T = new Date(lastBeginDate + "T05:00:00+0000");
            date2T.setDate(date2T.getDate() + toAddDays + 20);
            //alert(dateF + "-" + dateT + "-" + date2F + "-" +date2T);
            res = 'Your safe period is ' + dateF.toString().substring(0, 15) + " to " + dateT.toString().substring(0, 15)
                + " and " + date2F.toString().substring(0, 15) + " to " + date2T.toString().substring(0, 15);
        }
    }
    lastBeginDate = '';
    $('#res').text(res);
}

// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function calculator_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function calculator_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}