<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="msapplication-TileColor" content="#0061da">
    <meta name="theme-color" content="#1643a3">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href=resources/assets/favicon4x.png" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href=resources/assets/favicon4x.png"/>

    <!-- Title -->
    <title>SparkyAi - Parsed Files</title>

    <!--Font Awesome-->
    <link href="resources/assets/plugins/fontawesome-free/css/all.css" rel="stylesheet">

    <!-- Font Family -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600" rel="stylesheet">

    <!-- Dashboard Css -->
    <link href="resources/assets/css/dashboard.css" rel="stylesheet" />

    <!-- Custom scroll bar css-->
    <link href="resources/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

    <!-- Sidemenu Css -->
    <link href="resources/assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

    <!-- c3.js Charts Plugin -->
    <link href="resources/assets/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

    <!---Font icons-->
    <link href="resources/assets/plugins/iconfonts/plugin.css" rel="stylesheet" />

</head>
<body class="app">
<div id="global-loader" ><div class="showbox"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div></div>
<div class="page">
    <div class="page-main">
        <jsp:include page="includes/header.jsp"/>

        <!-- Horizantal menu-->
        <div class="ren-navbar" id="headerMenuCollapse">
            <div class="container">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="parsers">
                            <i class="fas fa-crosshairs"></i>
                            <span>Parsers</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " href="uploadFiles">
                            <i class="fas fa-file-upload"></i>
                            <span>Upload Files</span>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="parsedFiles">
                            <i class="fe fe-file-text" data-toggle="tooltip" title="" data-original-title="fe fe-file-text"></i>
                            <span>Parsed Files</span>
                        </a>
                    </li>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="unparsedFiles">
                            <i class="fe fe-alert-octagon" data-toggle="tooltip" title="" data-original-title="fe fe-alert-octagon"></i>
                            <span>Unparsed</span>
                            <span class="square-8"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Horizantal menu-->

        <div class="container">
            <div class="side-app">
                <div class="page-header">
                    <h4 class="page-title">Parsed Files</h4>
                    <ol class="breadcrumb">
                        <li class=""><a href="createParserUploadFiles" class="btn btn-danger">Create New Parser</a></li>
                    </ol>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="col-sm-12 col-md-6 text-left">Displaying list of parsed files</div>
                                <div class="col-sm-12 col-md-6 text-right">	<div class="input-group">
                                    <input type="text" class="form-control bg-white" placeholder="Search by uploaded file name">
                                    <div class="input-group-append ">
                                        <button type="button" class="btn-sm btn-primary">
                                            <i class="fas fa-search" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div></div>
                            </div>
                            <div class="table-responsive">
                                <table class="table card-table table-vcenter text-nowrap">
                                    <thead >
                                    <tr>
                                        <th>File ID</th>
                                        <th>Parser Used</th>
                                        <th>File Type</th>
                                        <th>Uploaded By</th>
                                        <th>Uploaded On</th>
                                        <th>Parsed On</th>
                                        <th>Accuracy</th>
                                        <th>Parsed File</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${parsedFileList}" var="parsedFile">
                                        <tr>
                                            <td><a href="downloadFile?dbId=${parsedFile.id}&fileExtension=.pdf&fileType=resultPdf"><i class="far fa-file-pdf text-red"></i></a>
                                                &nbsp;${parsedFile.id}</td>
                                            <td>${parsedFile.bestParser.name}</td>
                                            <td>Single</td>
                                            <td><%=session.getAttribute("email")%></td>
                                            <td>${parsedFile.uploadDate}</td>
                                            <td>${parsedFile.uploadDate}</td>
                                            <td>${parsedFile.accuracy}</td>
                                            <td><a href="downloadFile?dbId=${parsedFile.id}&fileExtension=.xls&fileType=resultXls"><i class="far fa-file-excel text-green ml-2"></i></a></td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <!-- table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--footer-->
        <footer class="footer fixed-bottom">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
                        Copyright &#169; 2018 <a href="#">sparkyai</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer-->
    </div>
</div>

<!-- Back to top -->
<a href="#top" id="back-to-top" style="display: inline;"><i class="fas fa-angle-up"></i></a>

<!-- Dashboard js -->
<script src="resources/assets/js/vendors/jquery-3.2.1.min.js"></script>
<script src="resources/assets/js/vendors/bootstrap.bundle.min.js"></script>
<script src="resources/assets/js/vendors/jquery.sparkline.min.js"></script>
<script src="resources/assets/js/vendors/selectize.min.js"></script>
<script src="resources/assets/js/vendors/jquery.tablesorter.min.js"></script>
<script src="resources/assets/js/vendors/circle-progress.min.js"></script>
<script src="resources/assets/plugins/rating/jquery.rating-stars.js"></script>
<!-- Side menu js -->
<script src="resources/assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

<!-- Custom scroll bar Js-->
<script src="resources/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

<!--Counters -->
<script src="resources/assets/plugins/counters/counterup.min.js"></script>
<script src="resources/assets/plugins/counters/waypoints.min.js"></script>

<!-- custom js -->
<script src="resources/assets/js/custom.js"></script>

</body>
</html>