-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pdfparser
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `parser`
--

DROP TABLE IF EXISTS `parser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(110) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parser_id_uindex` (`id`),
  UNIQUE KEY `parser_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parser`
--

LOCK TABLES `parser` WRITE;
/*!40000 ALTER TABLE `parser` DISABLE KEYS */;
INSERT INTO `parser` VALUES (1,'Test Parser 2',2,'2019-03-10 00:25:21'),(2,'Test Parser 1',2,'2019-03-10 00:27:47'),(3,'Test Parser 3',2,'2019-03-10 00:29:31');
/*!40000 ALTER TABLE `parser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result_mapping`
--

DROP TABLE IF EXISTS `result_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_index` int(11) DEFAULT NULL,
  `col_index` int(11) DEFAULT NULL,
  `is_static` int(11) DEFAULT NULL,
  `main_file_row_index` int(11) DEFAULT NULL,
  `main_file_col_index` int(11) DEFAULT NULL,
  `parser_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `result_mapping_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result_mapping`
--

LOCK TABLES `result_mapping` WRITE;
/*!40000 ALTER TABLE `result_mapping` DISABLE KEYS */;
INSERT INTO `result_mapping` VALUES (1,0,0,0,6,0,1),(2,0,1,0,6,1,1),(3,1,0,0,7,0,1),(4,1,1,0,7,1,1),(5,2,0,0,8,0,1),(6,2,1,0,-1,-1,1),(7,3,0,0,5,2,1),(8,3,1,0,-1,-1,1),(9,4,0,0,12,0,1),(10,4,1,0,12,1,1),(11,5,0,0,13,0,1),(12,5,1,0,13,1,1),(13,6,0,0,14,0,1),(14,7,0,0,15,0,1),(15,7,1,0,-1,-1,1),(16,9,0,0,5,0,1),(17,10,0,0,5,1,1),(18,0,0,0,5,0,3),(19,0,1,0,5,1,3),(20,1,0,0,6,0,3),(21,1,1,0,6,1,3),(22,2,0,0,7,0,3),(23,2,1,0,7,1,3),(24,3,0,0,5,2,3),(25,3,1,0,-1,-1,3),(26,4,0,0,12,0,3),(27,4,1,0,12,1,3),(28,5,0,0,12,0,3),(29,5,1,0,12,1,3),(30,6,0,0,13,0,3),(31,7,0,0,14,0,3),(32,7,1,0,14,1,3),(33,9,0,0,5,0,3),(34,10,0,0,5,1,3);
/*!40000 ALTER TABLE `result_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `training_file_set`
--

DROP TABLE IF EXISTS `training_file_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_file_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uploaded_by` int(11) NOT NULL DEFAULT '0',
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parser_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdf_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `training_file_set`
--

LOCK TABLES `training_file_set` WRITE;
/*!40000 ALTER TABLE `training_file_set` DISABLE KEYS */;
INSERT INTO `training_file_set` VALUES (2,2,'2019-03-10 00:23:44',1),(3,2,'2019-03-10 00:26:51',2),(4,2,'2019-03-10 00:28:07',3),(5,2,'2019-03-10 00:28:19',3),(6,2,'2019-03-10 00:29:20',3);
/*!40000 ALTER TABLE `training_file_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(110) NOT NULL,
  `password` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_uindex` (`id`),
  UNIQUE KEY `user_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@sparkyai.com','matrix007@123.in'),(2,'alamin@sparkyai.com','11'),(3,'user1@sparkyai.com','11'),(4,'user2@sparkyai.com','11'),(5,'user3@sparkyai.com','11');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-10  6:30:28
