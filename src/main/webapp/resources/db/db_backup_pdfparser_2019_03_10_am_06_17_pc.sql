

DROP TABLE IF EXISTS `parser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(110) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parser_id_uindex` (`id`),
  UNIQUE KEY `parser_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parser`
--

LOCK TABLES `parser` WRITE;
/*!40000 ALTER TABLE `parser` DISABLE KEYS */;
/*!40000 ALTER TABLE `parser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result_mapping`
--

DROP TABLE IF EXISTS `result_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_index` int(11) DEFAULT NULL,
  `col_index` int(11) DEFAULT NULL,
  `is_static` int(11) DEFAULT NULL,
  `main_file_row_index` int(11) DEFAULT NULL,
  `main_file_col_index` int(11) DEFAULT NULL,
  `parser_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `result_mapping_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result_mapping`
--

LOCK TABLES `result_mapping` WRITE;
/*!40000 ALTER TABLE `result_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `result_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `training_file_set`
--

DROP TABLE IF EXISTS `training_file_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_file_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uploaded_by` int(11) NOT NULL DEFAULT '0',
  `upload_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parser_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pdf_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `training_file_set`
--

LOCK TABLES `training_file_set` WRITE;
/*!40000 ALTER TABLE `training_file_set` DISABLE KEYS */;
/*!40000 ALTER TABLE `training_file_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(110) NOT NULL,
  `password` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_uindex` (`id`),
  UNIQUE KEY `user_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@sparkyai.com','matrix007@123.in'),(2,'alamin@sparkyai.com','11'),(3,'user1@sparkyai.com','11'),(4,'user2@sparkyai.com','11'),(5,'user3@sparkyai.com','11');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-10  6:17:34
